package kominfo.android.pelatihan.pelatihanandroid.kelasPengolahanData;

/**
 * Created by dera on 30/05/2016.
 */
public class DayaDengar {
    boolean j1, j2, j3, j4, j5;
    int usia;
    int jumlahJawabanTidak =0;
    public DayaDengar (int umur, boolean p1, boolean p2){
        j1 = p1;
        j2 = p2;
        usia = umur;
        if (j1 == false) {
            jumlahJawabanTidak++;
        }
        if (j2 == false) {
            jumlahJawabanTidak++;
        }
    }
    public DayaDengar (int umur, boolean p1, boolean p2, boolean p3){
        j1 = p1;
        j2 = p2;
        j3 = p3;
        usia = umur;
        if (j1 == false) {
            jumlahJawabanTidak++;
        }
        if (j2 == false) {
            jumlahJawabanTidak++;
        }
        if (j3 == false) {
            jumlahJawabanTidak++;
        }
    }
    public DayaDengar (int umur, boolean p1, boolean p2, boolean p3, boolean p4){
        j1 = p1;
        j2 = p2;
        j3 = p3;
        j4 = p4;
        usia = umur;
        if (j1 == false) {
            jumlahJawabanTidak++;
        }
        if (j2 == false) {
            jumlahJawabanTidak++;
        }
        if (j3 == false) {
            jumlahJawabanTidak++;
        }
        if (j4 == false) {
            jumlahJawabanTidak++;
        }
    }
    public DayaDengar (int umur, boolean p1, boolean p2, boolean p3, boolean p4, boolean p5){
        j1 = p1;
        j2 = p2;
        j3 = p3;
        j4 = p4;
        j5 = p5;
        usia = umur;
        if (j1 == false) {
            jumlahJawabanTidak++;
        }
        if (j2 == false) {
            jumlahJawabanTidak++;
        }
        if (j3 == false) {
            jumlahJawabanTidak++;
        }
        if (j4 == false) {
            jumlahJawabanTidak++;
        }
        if (j5 == false) {
            jumlahJawabanTidak++;
        }
    }
    public String[] statusDayaDengar (int jumlahTidak){
        //method untuk mendapatkan status daya dengar dengan input jumlah jawaban tidak pada tes daya dengar. Output ada 2 yaitu status daya dengar dan intervensi
        String statusDayaDengar="";
        String intervensi="";
        //status daya dengar jumlah tidak > 0 terjadi penyimpangan
        if (jumlahTidak > 0){
            statusDayaDengar = "Kemungkinan mengalami gangguan";
            intervensi = "Harus dibawa ke rumah sakit";
        }
        else{ statusDayaDengar = "Normal";}
        return new String[]{statusDayaDengar,intervensi};
    }
}
