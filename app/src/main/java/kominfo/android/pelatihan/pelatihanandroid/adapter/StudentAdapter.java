package kominfo.android.pelatihan.pelatihanandroid.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import kominfo.android.pelatihan.pelatihanandroid.R;
import kominfo.android.pelatihan.pelatihanandroid.model.Student;

/**
 * Created by erdearik on 5/2/16.
 */
public class StudentAdapter extends RecyclerView.Adapter<StudentAdapter.ViewHolder> {

    private Context mContext;
    private ArrayList<Student> allStudentArrayList;
    private static String today;
    private final String URL = "http://jerucommerce.com/get_all_students.php";

//    @Override
//    public void onClick(View view) {
////        Toast.makeText(mContext, "The Item Click is:" + get, Toast.LENGTH_SHORT).show();
//    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView id, name, code, create, update;
        public ImageView photo;
        public RelativeLayout rl;

        public ViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
//            photo = (ImageView) view.findViewById(R.id.foto);
//            instructions = (TextView) view.findViewById(R.id.instructions);
//            price = (TextView) view.findViewById(R.id.price);
//            category = (TextView) view.findViewById(R.id.category);
//            rl = (RelativeLayout) view.findViewById(R.id.rl);

        }

    }



    public StudentAdapter(Context mContext, ArrayList<Student> allStudentArrayList) {
        this.mContext = mContext;
        this.allStudentArrayList = allStudentArrayList;

        Calendar calendar = Calendar.getInstance();
        today = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_student_list, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Student allstudent = allStudentArrayList.get(position);
        holder.name.setText(allstudent.getName());
//        holder.ph.setText(allData.getPh());
//        holder.suhu.setText(allData.getSuhu());
//        holder.doo.setText(allData.getDoo());
//        holder.waktu.setText(getTimeStamp(allData.getWaktu()));
    }



    public static String getTimeStamp(String dateStr) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String timestamp = "";

        today = today.length() < 2 ? "0" + today : today;

        try {
            Date date = format.parse(dateStr);
            SimpleDateFormat todayFormat = new SimpleDateFormat("dd");
            String dateToday = todayFormat.format(date);
            format = dateToday.equals(today) ? new SimpleDateFormat("hh:mm a") : new SimpleDateFormat("dd LLL, hh:mm a");
            String date1 = format.format(date);
            timestamp = date1.toString();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return timestamp;
    }

    @Override
    public int getItemCount() {
        return allStudentArrayList.size();
    }

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private StudentAdapter.ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final StudentAdapter.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
}