package kominfo.android.pelatihan.pelatihanandroid.adapter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Base64;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.firebase.client.Firebase;
import com.firebase.client.Query;
import com.firebase.ui.FirebaseListAdapter;

import kominfo.android.pelatihan.pelatihanandroid.R;
import kominfo.android.pelatihan.pelatihanandroid.helper.Utils;
import kominfo.android.pelatihan.pelatihanandroid.model.Bayi;

/**
 * Created by erdearik on 6/5/16.
 */
public class BayiAdapter extends FirebaseListAdapter<Bayi> {

    private String mEncodedEmail;

    public BayiAdapter(Activity activity, Class<Bayi> modelClass, int modelLayout, Query ref, String encodedEmail) {
        super(activity, modelClass, modelLayout, ref);
        this.mEncodedEmail = encodedEmail;
        this.mActivity = activity;
    }

    @Override
    protected void populateView(View v, Bayi model) {
//        super.populateView(v, model);
        /**
         * Grab the needed Textivews and strings
         */
        TextView textViewListName = (TextView) v.findViewById(R.id.name);
        final TextView textViewUmur = (TextView) v.findViewById(R.id.umurr);
        final TextView textViewKondisi = (TextView) v.findViewById(R.id.kondisi);
        final ImageView img = (ImageView) v.findViewById(R.id.foto);

        textViewListName.setText(model.getNama());
        textViewKondisi.setText(model.getKondisi());
        textViewUmur.setText(model.getUmur());

        if(model.getPic() != null){
//            String decodeimage = Utils.decodeEmail(model.getPic());
//            byte[] decodedString = Base64.decode(decodeimage, Base64.DEFAULT);
//            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
//            img.setImageBitmap(decodedByte);
        } else {
//            img.getResources().getDrawable(R.drawable.image_default);
        }
    }
}
