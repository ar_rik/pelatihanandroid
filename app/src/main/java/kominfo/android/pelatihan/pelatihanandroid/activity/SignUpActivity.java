package kominfo.android.pelatihan.pelatihanandroid.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kominfo.android.pelatihan.pelatihanandroid.R;
import kominfo.android.pelatihan.pelatihanandroid.app.EndPoint;
import kominfo.android.pelatihan.pelatihanandroid.app.MyApplication;

public class SignUpActivity extends BaseActivity implements Validator.ValidationListener{
    @NotEmpty
    @Email
    @Bind(R.id.email)
    EditText regEmail;

    @NotEmpty
    @Bind(R.id.user)
    EditText regUser;

    @NotEmpty
    @Bind(R.id.mob)
    EditText mob;

    @Password
    @Bind(R.id.pass)
    EditText regPwd;

    public final static String TAG = SignUpActivity.class.getSimpleName();
    Validator validator;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);
        validator = new Validator(this);
        validator.setValidationListener(this);
    }

    @OnClick(R.id.login)
    public void login(){
        Intent i = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(i);
        finish();
    }

    @Override
    public void onValidationSucceeded() {
        final String e = regEmail.getText().toString();
        String u = regUser.getText().toString();
        final String p = regPwd.getText().toString();
        final ProgressDialog dia = ProgressDialog.show(this, null, "Alert Wait");
        StringRequest strReq = new StringRequest(Request.Method.POST,
                EndPoint.REGISTER, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);

                    // check for error
                    if (obj.getBoolean("error") == false) {
                        Toast.makeText(SignUpActivity.this, "User Succesfull to created", Toast.LENGTH_SHORT).show();
                        backToLogin();
                        dia.dismiss();

                    } else {
                        String hasil = obj.getString("message");
                        dia.dismiss();
                        Toast.makeText(SignUpActivity.this, hasil, Toast.LENGTH_SHORT).show();
                    }

                } catch (JSONException e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    Toast.makeText(SignUpActivity.this, "json parse error: " + e.getMessage(), Toast.LENGTH_SHORT).show();
                    dia.dismiss();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                Toast.makeText(SignUpActivity.this, "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
                dia.dismiss();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
//                params.put("user_id", MyApplication.getInstance().getPrefManager().getUser().getId());
                params.put("email", e);
                params.put("password",p);

                Log.e(TAG, "Params: " + params.toString());

                return params;
            }
        };
        //Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq);
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    @OnClick(R.id.signup)
    public void signup(){
        validator.validate();
    }
    private void backToLogin(){
        Toast.makeText(this, "SignUp Berhasil, Silahkan Login", Toast.LENGTH_SHORT).show();
        Intent i = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(i);
        finish();
    }
}
