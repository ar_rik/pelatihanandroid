package kominfo.android.pelatihan.pelatihanandroid.model;

/**
 * Created by erdearik on 5/2/16.
 */
public class DataBayi {
    private String id, nama, jk, tempat,  nama_ibu, tanggal, alamat;

    public DataBayi() {
    }

    public DataBayi(String id, String nama, String jk, String tempat, String nama_ibu, String tanggal, String alamat) {
        this.id = id;
        this.nama = nama;
        this.jk = jk;
        this.tempat = tempat;
        this.nama_ibu = nama_ibu;
        this.tanggal = tanggal;
        this.alamat = alamat;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getJk() {
        return jk;
    }

    public void setJk(String jk) {
        this.jk = jk;
    }

    public String getTempat() {
        return tempat;
    }

    public void setTempat(String tempat) {
        this.tempat = tempat;
    }

    public String getNama_ibu() {
        return nama_ibu;
    }

    public void setNama_ibu(String nama_ibu) {
        this.nama_ibu = nama_ibu;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }
}
