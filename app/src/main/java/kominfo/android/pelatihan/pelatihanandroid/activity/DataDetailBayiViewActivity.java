package kominfo.android.pelatihan.pelatihanandroid.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kominfo.android.pelatihan.pelatihanandroid.R;

public class DataDetailBayiViewActivity extends AppCompatActivity {

    @Bind(R.id.viewDet_bb)
    TextView bbb;

    @Bind(R.id.viewDet_bt)
    TextView btt;

    @Bind(R.id.viewDet_lp)
    TextView lp;

    @Bind(R.id.viewDet_tp)
    TextView tp;

    @Bind(R.id.nama)
    TextView namabayi;

    @Bind(R.id.umur)
    TextView umurbayi;

    private String id, bb, bt, umur, lingkar_kepala, tanggal, nama;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_detail_bayi_view);
        ButterKnife.bind(this);

        Intent i = getIntent();
        nama = i.getStringExtra("nama");
        id = i.getStringExtra("id");
        bb = i.getStringExtra("bb");
        bt = i.getStringExtra("bt");
        umur = i.getStringExtra("umur");
        lingkar_kepala = i.getStringExtra("lingkar_kepala");
        tanggal = i.getStringExtra("tanggal");

        setData();
    }

    private void setData() {
        namabayi.setText(nama);
        umurbayi.setText("Umur "+umur + " Bulan");
        bbb.setText(bb);
        btt.setText(bt);
        lp.setText(lingkar_kepala);
        tp.setText(tanggal);
    }

    @OnClick(R.id.btn_edit)
    public void edit(){
        Intent i = new Intent(getApplicationContext(), DataDetailBayiEditActivity.class);
        i.putExtra("nama",id);
        startActivity(i);
    }

}
