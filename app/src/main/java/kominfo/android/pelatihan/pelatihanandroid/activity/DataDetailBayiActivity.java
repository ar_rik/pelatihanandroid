package kominfo.android.pelatihan.pelatihanandroid.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import kominfo.android.pelatihan.pelatihanandroid.R;
import kominfo.android.pelatihan.pelatihanandroid.activity.databayi.DataBayiActivity;
import kominfo.android.pelatihan.pelatihanandroid.adapter.DataDetailBayiAdapter;
import kominfo.android.pelatihan.pelatihanandroid.app.EndPoint;
import kominfo.android.pelatihan.pelatihanandroid.app.MyApplication;
import kominfo.android.pelatihan.pelatihanandroid.model.DataDetailBayi;

public class DataDetailBayiActivity extends AppCompatActivity {

    private DataDetailBayiAdapter mAdapter;
    private RecyclerView recyclerView;
    private ArrayList<DataDetailBayi> allDataBayiDetailArrayList;
    private String TAG = DataBayiActivity.class.getSimpleName();
    private String idbayi, namabayi,lahir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_bayi);
        Log.d(TAG,"mulai");

        Intent intent = getIntent();
        idbayi = intent.getStringExtra("idbayi");
        namabayi = intent.getStringExtra("namabayi");
        lahir = intent.getStringExtra("lahir");

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        allDataBayiDetailArrayList = new ArrayList<>();
        mAdapter = new DataDetailBayiAdapter(this, allDataBayiDetailArrayList);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        recyclerView.addOnItemTouchListener(new DataDetailBayiAdapter.RecyclerTouchListener(this, recyclerView, new DataDetailBayiAdapter.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Toast.makeText(DataDetailBayiActivity.this, "click Data Detail "+position, Toast.LENGTH_SHORT).show();
                DataDetailBayi dataDetailBayi = allDataBayiDetailArrayList.get(position);
                Intent i = new Intent(getApplicationContext(), DataDetailBayiViewActivity.class);
                i.putExtra("nama",namabayi);
                i.putExtra("id",dataDetailBayi.getId());
                i.putExtra("bb",dataDetailBayi.getBb());
                i.putExtra("bt",dataDetailBayi.getTb());
                i.putExtra("umur", dataDetailBayi.getUmur());
                i.putExtra("lingkar_kepala",dataDetailBayi.getLingkar_kepala());
                i.putExtra("tanggal",dataDetailBayi.getTanggal_pemeriksaan());

                startActivity(i);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        fetchDataStudent();
    }

    private void fetchDataStudent() {
//        for(int i = 1; i < 9; i++){
//            DataDetailBayi ad = new DataDetailBayi();
//            ad.setId(String.valueOf(i));
//            ad.setUkur("Bayi "+i);
//            ad.setUmur(String.valueOf(i*3));
//            ad.setTanggal("IBU "+i);
//            allDataBayiDetailArrayList.add(ad);
//        }
        String endPoint = EndPoint.BAYI_ID_ALL.replace("_ID_", idbayi);
        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                endPoint, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "onResponse: " + response);
                try {
                    JSONObject obj = new JSONObject(response);

                    if (obj.getBoolean("error") == false) {
                        JSONArray data = obj.getJSONArray("databayi");
                        for (int i = 0; i < data.length(); i++) {
                            JSONObject dataObj = (JSONObject) data.get(i);
                            DataDetailBayi ad = new DataDetailBayi();
                            ad.setId(dataObj.getString("id"));
                            ad.setTanggal_pemeriksaan(dataObj.getString("tanggal_pemeriksaan"));
                            ad.setUmur(dataObj.getString("umur"));
                            ad.setBb(dataObj.getString("bb"));
                            ad.setTb(dataObj.getString("tb"));
                            ad.setLingkar_kepala(dataObj.getString("lingkar_kepala"));
                            allDataBayiDetailArrayList.add(ad);
                        }
                        Log.d(TAG, String.valueOf(allDataBayiDetailArrayList));
                    } else {
                        // error in fetching data
                        Toast.makeText(getApplicationContext(), "" + obj.getJSONObject("error").getString("message"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    Toast.makeText(getApplicationContext(), "Json parse error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
                mAdapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                Toast.makeText(getApplicationContext(), "Volley errror: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map headers = new HashMap();
                headers.put("Authorization", "746be99d4ef39262e059f3afb7a9cb4f");
                return headers;
            }
        };
//        Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(stringRequest);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.listdata,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.action_plus){
            Intent i = new Intent(getApplicationContext(), DataDetailBayiPlusActivity.class);
            i.putExtra("nama",namabayi);
            i.putExtra("lahir",lahir);
            i.putExtra("id",idbayi);
            startActivity(i);
        }
        return super.onOptionsItemSelected(item);
    }
}