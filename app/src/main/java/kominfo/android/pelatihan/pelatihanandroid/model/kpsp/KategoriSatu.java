package kominfo.android.pelatihan.pelatihanandroid.model.kpsp;

import java.util.HashMap;

/**
 * Created by erdearik on 6/7/16.
 */

// bulan 3 - 36 dan 54 - 72
public class KategoriSatu {
    private boolean p1, p2, p3, p4, p5, p6, p7, p8, p9, p10;
    private HashMap<String, Object> timestampLastChanged;
    private HashMap<String, Object> timestampCreated;
    private HashMap<String, Object> timestampLastChangedReverse;

    public HashMap<String, Object> getTimestampLastChanged() {
        return timestampLastChanged;
    }

    public void setTimestampLastChanged(HashMap<String, Object> timestampLastChanged) {
        this.timestampLastChanged = timestampLastChanged;
    }

    public HashMap<String, Object> getTimestampCreated() {
        return timestampCreated;
    }

    public void setTimestampCreated(HashMap<String, Object> timestampCreated) {
        this.timestampCreated = timestampCreated;
    }

    public HashMap<String, Object> getTimestampLastChangedReverse() {
        return timestampLastChangedReverse;
    }

    public void setTimestampLastChangedReverse(HashMap<String, Object> timestampLastChangedReverse) {
        this.timestampLastChangedReverse = timestampLastChangedReverse;
    }

    public KategoriSatu(boolean p1, boolean p2, boolean p3, boolean p4,
                        boolean p5, boolean p6, boolean p7, boolean p8,
                        boolean p9, boolean p10, HashMap<String, Object> timestampCreated) {
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
        this.p4 = p4;
        this.p5 = p5;
        this.p6 = p6;
        this.p7 = p7;
        this.p8 = p8;
        this.p9 = p9;
        this.p10 = p10;
    }

    public boolean getP1() {
        return p1;
    }

    public void setP1(boolean p1) {
        this.p1 = p1;
    }

    public boolean getP2() {
        return p2;
    }

    public void setP2(boolean p2) {
        this.p2 = p2;
    }

    public boolean getP3() {
        return p3;
    }

    public void setP3(boolean p3) {
        this.p3 = p3;
    }

    public boolean getP4() {
        return p4;
    }

    public void setP4(boolean p4) {
        this.p4 = p4;
    }

    public boolean getP5() {
        return p5;
    }

    public void setP5(boolean p5) {
        this.p5 = p5;
    }

    public boolean getP6() {
        return p6;
    }

    public void setP6(boolean p6) {
        this.p6 = p6;
    }

    public boolean getP7() {
        return p7;
    }

    public void setP7(boolean p7) {
        this.p7 = p7;
    }

    public boolean getP8() {
        return p8;
    }

    public void setP8(boolean p8) {
        this.p8 = p8;
    }

    public boolean getP9() {
        return p9;
    }

    public void setP9(boolean p9) {
        this.p9 = p9;
    }

    public boolean getP10() {
        return p10;
    }

    public void setP10(boolean p10) {
        this.p10 = p10;
    }
}
