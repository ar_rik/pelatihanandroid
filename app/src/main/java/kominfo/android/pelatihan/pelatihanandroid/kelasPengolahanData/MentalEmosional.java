package kominfo.android.pelatihan.pelatihanandroid.kelasPengolahanData;

/**
 * Created by dera on 30/05/2016.
 */
public class MentalEmosional {
    boolean p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12;
    public MentalEmosional(boolean j1,boolean j2,boolean j3,boolean j4,boolean j5,boolean j6,boolean j7,boolean j8,boolean j9,boolean j10,boolean j11,boolean j12){
        p1=j1;
        p2=j2;
        p3=j3;
        p4=j4;
        p5=j5;
        p6=j6;
        p7=j7;
        p8=j8;
        p9=j9;
        p10=j10;
        p11=j11;
        p12=j12;
    }
    public String[] statusMentalEmosional (){
        //method untuk mendapatkan status mental emosional. Output ada 2 yaitu status mental emosional dan intervensi
        String statusMentalEmosional="";
        String intervensi="";
        int jumlahYa = 0;
        if (p1 == true){
            jumlahYa++;
        }
        if (p2 == true){
            jumlahYa++;
        }
        if (p3 == true){
            jumlahYa++;
        }
        if (p4 == true){
            jumlahYa++;
        }
        if (p5 == true){
            jumlahYa++;
        }
        if (p6 == true){
            jumlahYa++;
        }
        if (p7 == true){
            jumlahYa++;
        }
        if (p8 == true){
            jumlahYa++;
        }
        if (p9 == true){
            jumlahYa++;
        }
        if (p10 == true){
            jumlahYa++;
        }
        if (p11 == true){
            jumlahYa++;
        }
        if (p12 == true){
            jumlahYa++;
        }

        if (jumlahYa == 1){
            statusMentalEmosional = "Kemungkinan mengalami gangguan";
            intervensi = "Harus konsultasi dengan tenaga medis &  evaluasi setelah 3 bulan";
        }
        else if (jumlahYa >= 2){
            statusMentalEmosional = "Kemungkinan mengalami gangguan, dengan jumlah Ya= "+ Integer.toString(jumlahYa);
            intervensi = "Harus dibawa ke rumah sakit dengan fasilitas kesehatan jiwa/tumbuh kembang anak";
        }
        else{
            statusMentalEmosional = "Normal";
        }
        return new String[]{statusMentalEmosional,intervensi};
    }
}
