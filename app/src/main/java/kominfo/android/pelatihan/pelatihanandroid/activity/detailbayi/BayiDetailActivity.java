package kominfo.android.pelatihan.pelatihanandroid.activity.detailbayi;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;

import butterknife.ButterKnife;
import butterknife.OnClick;
import kominfo.android.pelatihan.pelatihanandroid.R;
import kominfo.android.pelatihan.pelatihanandroid.activity.BaseActivity;
import kominfo.android.pelatihan.pelatihanandroid.activity.pengukuran.PengukuranActivity;
import kominfo.android.pelatihan.pelatihanandroid.activity.simulasi.SimulasiActivity;
import kominfo.android.pelatihan.pelatihanandroid.helper.Const;
import kominfo.android.pelatihan.pelatihanandroid.helper.Utils;
import kominfo.android.pelatihan.pelatihanandroid.model.Bayi;

public class BayiDetailActivity extends BaseActivity {

    private static final String LOG_TAG = BayiDetailActivity.class.getSimpleName();
    private String mListId;
    private Firebase mCurrentListRef;
    private ValueEventListener mCurrentListRefListener;
    private TextView nama, jk, tempat, tanggal;
    private ImageView imgDetail;
    private Bayi mBayi;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bayi_detail);
        ButterKnife.bind(this);
        /* Get the push ID from the extra passed by ShoppingListFragment */
        Intent intent = this.getIntent();
        mListId = intent.getStringExtra(Const.KEY_LIST_ID);
        if (mListId == null) {
            /* No point in continuing without a valid ID. */
            finish();
            return;
        }

        ActionBar bar = getSupportActionBar();
        bar.setTitle("Detail Bayi");
        bar.setDisplayHomeAsUpEnabled(true);

        mCurrentListRef = new Firebase(Const.FIREBASE_URL_USER_LISTS).child(mEncodedEmail).child(mListId);

        nama = (TextView)findViewById(R.id.txt_nama);
        jk = (TextView)findViewById(R.id.txt_jenis);
        tempat = (TextView)findViewById(R.id.txt_tempat_lahir);
        tanggal = (TextView)findViewById(R.id.txt_tgl_lahir);

        imgDetail = (ImageView)findViewById(R.id.img_detail);
    }

    @OnClick(R.id.btn_pengukuran)
    public void pengukuran(){
        Intent intent = new Intent(getApplicationContext(), PengukuranActivity.class);
        intent.putExtra(Const.KEY_LIST_ID, mListId);
        startActivity(intent);
    }

    @OnClick(R.id.btn_simulasi)
    public void simulasi(){
        Intent intent = new Intent(getApplicationContext(), SimulasiActivity.class);
        startActivity(intent);
    }
    @Override
    protected void onResume() {
        super.onResume();
        /**
         * Save the most recent version of current shopping list into mShoppingList instance
         * variable an update the UI to match the current list.
         */
        mCurrentListRefListener = mCurrentListRef.addValueEventListener(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot snapshot) {

                /**
                 * Saving the most recent version of current shopping list into mShoppingList if present
                 * finish() the activity if the list is null (list was removed or unshared by it's owner
                 * while current user is in the list details activity)
                 */
                Bayi dataBayi = snapshot.getValue(Bayi.class);

                if (dataBayi == null) {
                    finish();
                    /**
                     * Make sure to call return, otherwise the rest of the method will execute,
                     * even after calling finish.
                     */
                    return;
                }
                mBayi = dataBayi;
                nama.setText(Html.fromHtml(mBayi.getNama()));
                if(mBayi.getJenisKelamin() == Const.LAKI) jk.setText(Html.fromHtml("Laki - Laki"));
                else jk.setText(Html.fromHtml("Perempuan"));
                tempat.setText(Html.fromHtml(mBayi.getTempatLahir()));
                tanggal.setText(Html.fromHtml(mBayi.getUmur()));

                if(mBayi.getPic() != null){
//                    String dataaa = Utils.decodeEmail(mBayi.getPic());
//                    byte[] decodedString = Base64.decode(dataaa, Base64.DEFAULT);
//                    Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
//                    imgDetail.setImageBitmap(decodedByte);
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                Log.e(LOG_TAG,
                        getString(R.string.log_error_the_read_failed) +
                                firebaseError.getMessage());
            }
        });
    }
}
