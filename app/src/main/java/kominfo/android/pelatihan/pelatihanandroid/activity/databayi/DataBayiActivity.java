package kominfo.android.pelatihan.pelatihanandroid.activity.databayi;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.firebase.client.Firebase;
import com.firebase.client.Query;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import kominfo.android.pelatihan.pelatihanandroid.R;
import kominfo.android.pelatihan.pelatihanandroid.activity.BaseActivity;
import kominfo.android.pelatihan.pelatihanandroid.activity.detailbayi.BayiDetailActivity;
import kominfo.android.pelatihan.pelatihanandroid.adapter.BayiAdapter;
import kominfo.android.pelatihan.pelatihanandroid.adapter.DataBayiAdapter;
import kominfo.android.pelatihan.pelatihanandroid.app.EndPoint;
import kominfo.android.pelatihan.pelatihanandroid.app.MyApplication;
import kominfo.android.pelatihan.pelatihanandroid.helper.Const;
import kominfo.android.pelatihan.pelatihanandroid.model.Bayi;
import kominfo.android.pelatihan.pelatihanandroid.model.DataBayi;

public class DataBayiActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener{

    private DataBayiAdapter mAdapter;
    private RecyclerView recyclerView;
    private ArrayList<DataBayi> allDataBayiArrayList;
    private String TAG = DataBayiActivity.class.getSimpleName();
    private SwipeRefreshLayout refreshLayout;


    private ListView mListView;

    public DataBayiActivity() {
        /* Required empty public constructor */
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_bayi);
        Log.d(TAG,"mulai");

        mListView = (ListView) findViewById(R.id.list_view_active_lists);

        /**
         * Set interactive bits, such as click events and adapters
         */
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Bayi selectedList = mActiveListAdapter.getItem(position);
                if (selectedList != null) {
                    Intent intent = new Intent(getApplicationContext(), BayiDetailActivity.class);
                    /* Get the list ID using the adapter's get ref method to get the Firebase
                     * ref and then grab the key.
                     */
                    String listId = mActiveListAdapter.getRef(position).getKey();
                    idBayi = listId;
                    intent.putExtra(Const.KEY_LIST_ID, listId);
                    /* Starts an active showing the details for the selected list */
                    startActivity(intent);
                }
            }
        });

    }

    /**
     * Updates the order of mListView onResume to handle sortOrderChanges properly
     */
    @Override
    public void onResume() {
        super.onResume();
        final SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(this);
        String sortOrder = sharedPref.getString(Const.KEY_PREF_SORT_ORDER_LISTS, Const.ORDER_BY_KEY);

        Query orderedActiveUserListsRef;
        Firebase activeListsRef = new Firebase(Const.FIREBASE_URL_USER_LISTS)
                .child(mEncodedEmail);
        /**
         * Sort active lists by "date created"
         * if it's been selected in the SettingsActivity
         */
        if (sortOrder.equals(Const.ORDER_BY_KEY)) {
            orderedActiveUserListsRef = activeListsRef.orderByKey();
        } else {

            /**
             * Sort active by lists by name or datelastChanged. Otherwise
             * depending on what's been selected in SettingsActivity
             */

            orderedActiveUserListsRef = activeListsRef.orderByChild(sortOrder);
        }

        /**
         * Create the adapter with selected sort order
         */
        mActiveListAdapter = new BayiAdapter(this, Bayi.class,
                R.layout.activity_data_bayi_list, orderedActiveUserListsRef,
                mEncodedEmail);

        /**
         * Set the adapter to the mListView
         */
        mListView.setAdapter(mActiveListAdapter);
    }

    /**
     * Cleanup the adapter when activity is paused.
     */
    @Override
    public void onPause() {
        super.onPause();
        mActiveListAdapter.cleanup();
    }

    private void fetchDataStudent() {
//        for(int i = 1; i < 9; i++){
//            DataBayi ad = new DataBayi();
//            ad.setId(String.valueOf(i));
//            ad.setNama("Bayi "+i);
//            ad.setUmur(String.valueOf(i*3+5));
//            ad.setIbu("IBU "+i);
//            ad.setAlamat("jalan " + 1);
//            allDataBayiArrayList.add(ad);
//        }

        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                EndPoint.DATA_BAYI, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "onResponse: " + response);
                try {
                    JSONObject obj = new JSONObject(response);

                    if (obj.getBoolean("error") == false) {
                        JSONArray data = obj.getJSONArray("bayi");
                        for (int i = 0; i < data.length(); i++) {
                            JSONObject dataObj = (JSONObject) data.get(i);
                            DataBayi ad = new DataBayi();
                            ad.setId(dataObj.getString("id"));
                            ad.setNama(dataObj.getString("nama"));
                            ad.setJk(dataObj.getString("jk"));
                            ad.setTempat(dataObj.getString("tempat"));
                            ad.setNama_ibu(dataObj.getString("nama_ibu"));
                            ad.setAlamat(dataObj.getString("alamat"));
                            ad.setTanggal(dataObj.getString("tanggal"));
                            allDataBayiArrayList.add(ad);
                        }
                        Log.d(TAG, String.valueOf(allDataBayiArrayList));
                    } else {
                        // error in fetching data
                        Toast.makeText(getApplicationContext(), "" + obj.getJSONObject("error").getString("message"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    Toast.makeText(getApplicationContext(), "Json parse error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
                mAdapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                Toast.makeText(getApplicationContext(), "Volley errror: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map headers = new HashMap();
                headers.put("Authorization", "746be99d4ef39262e059f3afb7a9cb4f");
                return headers;
            }
        };
//        Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(stringRequest);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.listdata,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.action_plus){
            Intent i = new Intent(getApplicationContext(), DataBayiPlusActivity.class);
            startActivity(i);
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRefresh() {
        refreshLayout.setColorSchemeColors(R.color.colorPrimary,
                R.color.colorAccent,
                R.color.colorSecondGreen);
        fetchDataStudent();
        refreshLayout.setRefreshing(false);
    }

}
