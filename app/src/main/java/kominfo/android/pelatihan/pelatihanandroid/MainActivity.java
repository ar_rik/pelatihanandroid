package kominfo.android.pelatihan.pelatihanandroid;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import kominfo.android.pelatihan.pelatihanandroid.adapter.StudentAdapter;
import kominfo.android.pelatihan.pelatihanandroid.app.EndPoint;
import kominfo.android.pelatihan.pelatihanandroid.app.MyApplication;
import kominfo.android.pelatihan.pelatihanandroid.model.Student;

public class MainActivity extends AppCompatActivity{

    private StudentAdapter mAdapter;
    private RecyclerView recyclerView;
    private ArrayList<Student> allStudentArrayList;
    private String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(TAG,"mulai");
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        allStudentArrayList = new ArrayList<>();
        mAdapter = new StudentAdapter(this, allStudentArrayList);

        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);
        recyclerView.addOnItemTouchListener(new StudentAdapter.RecyclerTouchListener(this, recyclerView, new StudentAdapter.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Toast.makeText(MainActivity.this, "click "+position, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        fetchDataStudent();
    }

    private void fetchDataStudent() {
        Log.d(TAG,"mulai fetch");
        StringRequest stringRequest = new StringRequest(Request.Method.GET,
                EndPoint.BASE_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e(TAG, "onResponse: " + response);
                try {
                    JSONObject obj = new JSONObject(response);

                    if (obj.getInt("success") == 1) {
                        JSONArray data = obj.getJSONArray("students");
                        for (int i = 0; i < data.length(); i++) {
                            JSONObject dataObj = (JSONObject) data.get(i);
                            Student ad = new Student();
                            ad.setId(dataObj.getString("sid"));
                            ad.setName(dataObj.getString("name"));
                            ad.setCode(dataObj.getString("code"));
                            ad.setCreate(dataObj.getString("created_at"));
                            ad.setUpdate(dataObj.getString("updated_at"));
                            allStudentArrayList.add(ad);
                        }
                        Log.d(TAG, String.valueOf(allStudentArrayList));
                    } else {
                        // error in fetching data
                        Toast.makeText(getApplicationContext(), "" + obj.getJSONObject("error").getString("message"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    Log.e(TAG, "json parsing error: " + e.getMessage());
                    Toast.makeText(getApplicationContext(), "Json parse error: " + e.getMessage(), Toast.LENGTH_LONG).show();
                }
                mAdapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error: " + error.getMessage() + ", code: " + networkResponse);
                Toast.makeText(getApplicationContext(), "Volley errror: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map headers = new HashMap();

                return headers;
            }
        };
        //Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(stringRequest);
    }

}
