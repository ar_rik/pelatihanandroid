package kominfo.android.pelatihan.pelatihanandroid.kelasPengolahanData;

/**
 * Created by dera on 30/05/2016.
 */
public class Perkembangan {
    //class untuk Perkembangan yang mempunyai 4 method: method untuk status perkembangan dan intervensi ; method untuk mendapatkan jumlah tidak gerak halus; method untuk mendapatkan jumlah tidak gerak kasar; method untuk mendapatkan jumlah tidak bicara bahasa; method untuk mendapatkan jumlah tidak sosial mandiri
    boolean j1, j2, j3, j4, j5, j6, j7, j8, j9, j10;
    int usia;

    public Perkembangan(int umur, boolean p1, boolean p2, boolean p3, boolean p4, boolean p5, boolean p6, boolean p7, boolean p8, boolean p9, boolean p10) {
        j1 = p1;
        j2 = p2;
        j3 = p3;
        j4 = p4;
        j5 = p5;
        j6 = p6;
        j7 = p7;
        j8 = p8;
        j9 = p9;
        j10 = p10;
        usia = umur;
    }
    public Perkembangan(int umur, boolean p1, boolean p2, boolean p3, boolean p4, boolean p5, boolean p6, boolean p7, boolean p8, boolean p9) {
        //untuk umur 42 dan 48
        j1 = p1;
        j2 = p2;
        j3 = p3;
        j4 = p4;
        j5 = p5;
        j6 = p6;
        j7 = p7;
        j8 = p8;
        j9 = p9;
        usia = umur;
    }

    public String[] statusPerkembangan() {
        int jumlahJawabanTidak = 0;
        String statusPerkembangan = "";
        String intervensi = "";
        if (j1 == false) {
            jumlahJawabanTidak++;
        }
        if (j2 == false) {
            jumlahJawabanTidak++;
        }
        if (j3 == false) {
            jumlahJawabanTidak++;
        }
        if (j4 == false) {
            jumlahJawabanTidak++;
        }
        if (j5 == false) {
            jumlahJawabanTidak++;
        }
        if (j6 == false) {
            jumlahJawabanTidak++;
        }
        if (j7 == false) {
            jumlahJawabanTidak++;
        }
        if (j8 == false) {
            jumlahJawabanTidak++;
        }
        if (j9 == false) {
            jumlahJawabanTidak++;
        }
        if (j10 == false) {
            jumlahJawabanTidak++;
        }

        if ((jumlahJawabanTidak == 3) || (jumlahJawabanTidak == 2)) {
            statusPerkembangan = "Meragukan";
            intervensi = "Melakukan stimulus lebih intens pada jenis keterlambatan & melakukan tes kembali 2 minggu kemudian";
        } else if (jumlahJawabanTidak >= 4) {
            statusPerkembangan = "Terdapat penyimpangan";
            intervensi = "Harus dibawa ke rumah sakit";
        } else {
            statusPerkembangan = "Perkembangan Sesuai";
        }
        return new String[] {statusPerkembangan, intervensi};
    }

    public int tidakGerakHalus() {
        int tidakGerakHalus = 0;
        if (usia == 3) {
            if (j4 == false) {
                tidakGerakHalus++;
            }
            if (j5 == false) {
                tidakGerakHalus++;
            }
        } else if (usia == 6) {
            if (j1 == false) {
                tidakGerakHalus++;
            }
            if (j3 == false) {
                tidakGerakHalus++;
            }
            if (j8 == false) {
                tidakGerakHalus++;
            }
            if (j9 == false) {
                tidakGerakHalus++;
            }
        } else if (usia == 9) {
            if (j2 == false) {
                tidakGerakHalus++;
            }
            if (j3 == false) {
                tidakGerakHalus++;
            }
            if (j4 == false) {
                tidakGerakHalus++;
            }
            if (j6 == false) {
                tidakGerakHalus++;
            }
        }
        else if (usia == 12) {
            if (j2 == false) {
                tidakGerakHalus++;
            }
            if (j7 == false) {
                tidakGerakHalus++;
            }
            if (j10 = false) {
                tidakGerakHalus++;
            }
        }
        else if (usia == 15) {
            if (j1 == false) {
                tidakGerakHalus++;
            }
            if (j10 == false) {
                tidakGerakHalus++;
            }
        }
        else if (usia == 18) {
            if (j8 == false) {
                tidakGerakHalus++;
            }
            if (j9 == false) {
                tidakGerakHalus++;
            }
        }
        else if (usia == 21) {
            if (j4 == false) {
                tidakGerakHalus++;
            }
            if (j5 == false) {
                tidakGerakHalus++;
            }
            if (j8 = false) {
                tidakGerakHalus++;
            }
        }
        else if (usia == 24) {
            if (j2 == false) {
                tidakGerakHalus++;
            }
        }
        else if (usia == 30) {
            if (j7 == false) {
                tidakGerakHalus++;
            }
            if (j8 == false) {
                tidakGerakHalus++;
            }
        }
        else if (usia == 36) {
            if (j2 == false) {
                tidakGerakHalus++;
            }
            if (j1 == false) {
                tidakGerakHalus++;
            }
            if (j7 = false) {
                tidakGerakHalus++;
            }
        }
        else if (usia == 42) {
            if (j6 == false) {
                tidakGerakHalus++;
            }
            if (j7 == false) {
                tidakGerakHalus++;
            }
        }
        else if (usia == 48) {
            if (j6 == false) {
                tidakGerakHalus++;
            }
            if (j5 == false) {
                tidakGerakHalus++;
            }
        }
        else if (usia == 54) {
            if (j1 == false) {
                tidakGerakHalus++;
            }
            if (j8 == false) {
                tidakGerakHalus++;
            }
            if (j9 == false) {
                tidakGerakHalus++;
            }
        }
        else if (usia == 60) {
            if (j4 == false) {
                tidakGerakHalus++;
            }
            if (j5 == false) {
                tidakGerakHalus++;
            }
        }
        else if (usia == 66) {
            if (j1 == false) {
                tidakGerakHalus++;
            }
            if (j7 == false) {
                tidakGerakHalus++;
            }
            if (j8 == false) {
                tidakGerakHalus++;
            }
        }
        else if (usia == 72) {
            if (j4 == false) {
                tidakGerakHalus++;
            }
            if (j5 == false) {
                tidakGerakHalus++;
            }
            if (j9 == false) {
                tidakGerakHalus++;
            }
        }
        return tidakGerakHalus;
    }
    public int tidakGerakKasar() {
        int tidakGerakKasar = 0;
        if (usia == 3) {
            if (j1 == false) {
                tidakGerakKasar++;
            }
            if (j7 == false) {
                tidakGerakKasar++;
            }
            if (j8 == false) {
                tidakGerakKasar++;
            }
            if (j9 == false) {
                tidakGerakKasar++;
            }
        } else if (usia == 6) {
            if (j2 == false) {
                tidakGerakKasar++;
            }
            if (j4 == false) {
                tidakGerakKasar++;
            }
            if (j6 == false) {
                tidakGerakKasar++;
            }
            if (j10 == false) {
                tidakGerakKasar++;
            }
        } else if (usia == 9) {
            if (j1 == false) {
                tidakGerakKasar++;
            }
            if (j5 == false) {
                tidakGerakKasar++;
            }
            if (j7 == false) {
                tidakGerakKasar++;
            }
        }
        else if (usia == 12) {
            if (j3 == false) {
                tidakGerakKasar++;
            }
            if (j5 == false) {
                tidakGerakKasar++;
            }
            if (j8 = false) {
                tidakGerakKasar++;
            }
        }
        else if (usia == 15) {
            if (j2 == false) {
                tidakGerakKasar++;
            }
            if (j5 == false) {
                tidakGerakKasar++;
            }
            if (j6 == false) {
                tidakGerakKasar++;
            }
            if (j7 == false) {
                tidakGerakKasar++;
            }
            if (j9 == false) {
                tidakGerakKasar++;
            }
        }
        else if (usia == 18) {
            if (j3 == false) {
                tidakGerakKasar++;
            }
            if (j5 == false) {
                tidakGerakKasar++;
            }
            if (j4 == false) {
                tidakGerakKasar++;
            }
            if (j7 == false) {
                tidakGerakKasar++;
            }
        }
        else if (usia == 21) {
            if (j1 == false) {
                tidakGerakKasar++;
            }
            if (j3 == false) {
                tidakGerakKasar++;
            }
            if (j10 == false) {
                tidakGerakKasar++;
            }
        }
        else if (usia == 24) {
            if (j4 == false) {
                tidakGerakKasar++;
            }
            if (j6 == false) {
                tidakGerakKasar++;
            }
            if (j10 == false) {
                tidakGerakKasar++;
            }
        }
        else if (usia == 30) {
            if (j2 == false) {
                tidakGerakKasar++;
            }
            if (j6 == false) {
                tidakGerakKasar++;
            }
        }
        else if (usia == 36) {
            if (j5 == false) {
                tidakGerakKasar++;
            }
            if (j8 == false) {
                tidakGerakKasar++;
            }
            if (j10 == false) {
                tidakGerakKasar++;
            }
        }
        else if (usia == 42) {
            if (j2 == false) {
                tidakGerakKasar++;
            }
            if (j5 == false) {
                tidakGerakKasar++;
            }
            if (j4 == false) {
                tidakGerakKasar++;
            }
        }
        else if (usia == 48) {
            if (j1 == false) {
                tidakGerakKasar++;
            }
            if (j3 == false) {
                tidakGerakKasar++;
            }
            if (j4 == false) {
                tidakGerakKasar++;
            }
        }
        else if (usia == 54) {
            if (j7 == false) {
                tidakGerakKasar++;
            }
        }
        else if (usia == 60) {
            if (j3 == false) {
                tidakGerakKasar++;
            }
            if (j9 == false) {
                tidakGerakKasar++;
            }
        }
        else if (usia == 66) {
            if (j10 == false) {
                tidakGerakKasar++;
            }
            if (j5 == false) {
                tidakGerakKasar++;
            }
        }
        else if (usia == 72) {
            if (j2 == false) {
                tidakGerakKasar++;
            }
            if (j7 == false) {
                tidakGerakKasar++;
            }
            if (j8 == false) {
                tidakGerakKasar++;
            }
        }
        return tidakGerakKasar;
    }
    public int tidakBicaraBahasa() {
        int tidakBicaraBahasa = 0;
        if (usia == 3) {
            if (j3 == false) {
                tidakBicaraBahasa++;
            }
            if (j10 == false) {
                tidakBicaraBahasa++;
            }
        } else if (usia == 6) {
            if (j5 == false) {
                tidakBicaraBahasa++;
            }
        } else if (usia == 9) {
            if (j9 == false) {
                tidakBicaraBahasa++;
            }
        }
        else if (usia == 12) {
            if (j4 == false) {
                tidakBicaraBahasa++;
            }
            if (j9 == false) {
                tidakBicaraBahasa++;
            }
        }
        else if (usia == 15) {
            if (j4 == false) {
                tidakBicaraBahasa++;
            }
        }
        else if (usia == 18) {
            if (j2 == false) {
                tidakBicaraBahasa++;
            }
        }
        else if (usia == 21) {
            if (j9 == false) {
                tidakBicaraBahasa++;
            }
        }
        else if (usia == 24) {
            if (j3 == false) {
                tidakBicaraBahasa++;
            }
            if (j7 == false) {
                tidakBicaraBahasa++;
            }
            if (j9 == false) {
                tidakBicaraBahasa++;
            }
        }
        else if (usia == 30) {
            if (j3 == false) {
                tidakBicaraBahasa++;
            }
            if (j5 == false) {
                tidakBicaraBahasa++;
            }
            if (j9 == false) {
                tidakBicaraBahasa++;
            }
            if (j10 == false) {
                tidakBicaraBahasa++;
            }
        }
        else if (usia == 36) {
            if (j3 == false) {
                tidakBicaraBahasa++;
            }
            if (j4 == false) {
                tidakBicaraBahasa++;
            }
            if (j6 == false) {
                tidakBicaraBahasa++;
            }
        }
        else if (usia == 48) {
            if (j9 == false) {
                tidakBicaraBahasa++;
            }
        }
        else if (usia == 54) {
            if (j4 == false) {
                tidakBicaraBahasa++;
            }
            if (j5 == false) {
                tidakBicaraBahasa++;
            }
            if (j10 == false) {
                tidakBicaraBahasa++;
            }
        }
        else if (usia == 60) {
            if (j1 == false) {
                tidakBicaraBahasa++;
            }
            if (j6 == false) {
                tidakBicaraBahasa++;
            }
            if (j8 == false) {
                tidakBicaraBahasa++;
            }
        }
        else if (usia == 66) {
            if (j2 == false) {
                tidakBicaraBahasa++;
            }
            if (j4 == false) {
                tidakBicaraBahasa++;
            }
            if (j9 == false) {
                tidakBicaraBahasa++;
            }
        }
        else if (usia == 72) {
            if (j1 == false) {
                tidakBicaraBahasa++;
            }
            if (j10 == false) {
                tidakBicaraBahasa++;
            }
        }
        return tidakBicaraBahasa;
    }
    public int tidakSosialMandiri() {
        int tidakSosialMandiri = 0;
        if (usia == 3) {
            if (j2 == false) {
                tidakSosialMandiri++;
            }
            if (j6 == false) {
                tidakSosialMandiri++;
            }
        } else if (usia == 6) {
            if (j7 == false) {
                tidakSosialMandiri++;
            }
        } else if (usia == 9) {
            if (j8 == false) {
                tidakSosialMandiri++;
            }
            if (j10 == false) {
                tidakSosialMandiri++;
            }
        }
        else if (usia == 12) {
            if (j1 == false) {
                tidakSosialMandiri++;
            }
            if (j6 == false) {
                tidakSosialMandiri++;
            }
        }
        else if (usia == 15) {
            if (j3 == false) {
                tidakSosialMandiri++;
            }
            if (j8 == false) {
                tidakSosialMandiri++;
            }
        }
        else if (usia == 18) {
            if (j1 == false) {
                tidakSosialMandiri++;
            }
            if (j6 == false) {
                tidakSosialMandiri++;
            }
            if (j9 == false) {
                tidakSosialMandiri++;
            }
            if (j10 == false) {
                tidakSosialMandiri++;
            }
        }
        else if (usia == 21) {
            if (j2 == false) {
                tidakSosialMandiri++;
            }
            if (j6 == false) {
                tidakSosialMandiri++;
            }
            if (j7 == false) {
                tidakSosialMandiri++;
            }
        }
        else if (usia == 24) {
            if (j1 == false) {
                tidakSosialMandiri++;
            }
            if (j5 == false) {
                tidakSosialMandiri++;
            }
            if (j8 == false) {
                tidakSosialMandiri++;
            }
        }
        else if (usia == 30) {
            if (j1 == false) {
                tidakSosialMandiri++;
            }
            if (j4 == false) {
                tidakSosialMandiri++;
            }
        }
        else if (usia == 36) {
            if (j9 == false) {
                tidakSosialMandiri++;
            }
        }
        else if (usia == 42) {
            if (j1 == false) {
                tidakSosialMandiri++;
            }
            if (j3 == false) {
                tidakSosialMandiri++;
            }
            if (j8 == false) {
                tidakSosialMandiri++;
            }
            if (j9 == false) {
                tidakSosialMandiri++;
            }
        }
        else if (usia == 48) {
            if (j2 == false) {
                tidakSosialMandiri++;
            }
            if (j7 == false) {
                tidakSosialMandiri++;
            }
            if (j8 == false) {
                tidakSosialMandiri++;
            }
        }
        else if (usia == 54) {
            if (j2 == false) {
                tidakSosialMandiri++;
            }
            if (j3 == false) {
                tidakSosialMandiri++;
            }
            if (j6 == false) {
                tidakSosialMandiri++;
            }
        }
        else if (usia == 60) {
            if (j2 == false) {
                tidakSosialMandiri++;
            }
            if (j7 == false) {
                tidakSosialMandiri++;
            }
            if (j10 == false) {
                tidakSosialMandiri++;
            }
        }
        else if (usia == 66) {
            if (j3 == false) {
                tidakSosialMandiri++;
            }
            if (j6 == false) {
                tidakSosialMandiri++;
            }
        }
        else if (usia == 72) {
            if (j3 == false) {
                tidakSosialMandiri++;
            }
            if (j6 == false) {
                tidakSosialMandiri++;
            }
        }
        return tidakSosialMandiri;
    }

}
