package kominfo.android.pelatihan.pelatihanandroid.activity.soal;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ServerValue;

import java.util.HashMap;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kominfo.android.pelatihan.pelatihanandroid.R;
import kominfo.android.pelatihan.pelatihanandroid.activity.pengukuran.PengukuranActivity;
import kominfo.android.pelatihan.pelatihanandroid.helper.Const;
import kominfo.android.pelatihan.pelatihanandroid.helper.Utils;
import kominfo.android.pelatihan.pelatihanandroid.model.kpsp.KategoriSatu;

public class SoalBulanDuaBelasActivity extends AppCompatActivity {
    @Bind(R.id.yes_1)
    RadioButton yes1;
    @Bind(R.id.yes_2)
    RadioButton yes2;
    @Bind(R.id.yes_3)
    RadioButton yes3;
    @Bind(R.id.yes_4)
    RadioButton yes4;
    @Bind(R.id.yes_5)
    RadioButton yes5;
    @Bind(R.id.yes_6)
    RadioButton yes6;
    @Bind(R.id.yes_7)
    RadioButton yes7;
    @Bind(R.id.yes_8)
    RadioButton yes8;
    @Bind(R.id.yes_9)
    RadioButton yes9;
    @Bind(R.id.yes_10)
    RadioButton yes10;

    @Bind(R.id.no_1)
    RadioButton no1;
    @Bind(R.id.no_2)
    RadioButton no2;
    @Bind(R.id.no_3)
    RadioButton no3;
    @Bind(R.id.no_4)
    RadioButton no4;
    @Bind(R.id.no_5)
    RadioButton no5;
    @Bind(R.id.no_6)
    RadioButton no6;
    @Bind(R.id.no_7)
    RadioButton no7;
    @Bind(R.id.no_8)
    RadioButton no8;
    @Bind(R.id.no_9)
    RadioButton no9;
    @Bind(R.id.no_10)
    RadioButton no10;


    @Bind(R.id.textSoal_1)
    TextView soal1;
    @Bind(R.id.textSoal_2)
    TextView soal2;
    @Bind(R.id.textSoal_3)
    TextView soal3;
    @Bind(R.id.textSoal_4)
    TextView soal4;
    @Bind(R.id.textSoal_5)
    TextView soal5;
    @Bind(R.id.textSoal_6)
    TextView soal6;
    @Bind(R.id.textSoal_7)
    TextView soal7;
    @Bind(R.id.textSoal_8)
    TextView soal8;
    @Bind(R.id.textSoal_9)
    TextView soal9;
    @Bind(R.id.textSoal_10)
    TextView soal10;

    @Bind(R.id.img_kpsp_1)
    ImageView img1;
    @Bind(R.id.img_kpsp_2)
    ImageView img2;
    @Bind(R.id.img_kpsp_3)
    ImageView img3;
    @Bind(R.id.img_kpsp_4)
    ImageView img4;
    @Bind(R.id.img_kpsp_5)
    ImageView img5;
    @Bind(R.id.img_kpsp_6)
    ImageView img6;
    @Bind(R.id.img_kpsp_7)
    ImageView img7;
    @Bind(R.id.img_kpsp_8)
    ImageView img8;
    @Bind(R.id.img_kpsp_9)
    ImageView img9;
    @Bind(R.id.img_kpsp_10)
    ImageView img10;

    private String namaa, lahir, id;
    private boolean ans1, ans2, ans3, ans4, ans5, ans6, ans7, ans8, ans9, ans10;
    private String mListId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_soal_kpsp);
        ButterKnife.bind(this);
        Intent i = getIntent();
        namaa = i.getStringExtra("namabayi");
        lahir = i.getStringExtra("lahir");
        id = i.getStringExtra("idbayi");
//        Intent intent = this.getIntent();
        mListId = i.getStringExtra(Const.KEY_LIST_ID);
        initScreen();
    }

    private void initScreen() {
        soal1.setText(getResources().getStringArray(R.array.pengukuran_bulan_12)[0]);
        soal2.setText(getResources().getStringArray(R.array.pengukuran_bulan_12)[1]);
        soal3.setText(getResources().getStringArray(R.array.pengukuran_bulan_12)[2]);
        soal4.setText(getResources().getStringArray(R.array.pengukuran_bulan_12)[3]);
        soal5.setText(getResources().getStringArray(R.array.pengukuran_bulan_12)[4]);
        soal6.setText(getResources().getStringArray(R.array.pengukuran_bulan_12)[5]);
        soal7.setText(getResources().getStringArray(R.array.pengukuran_bulan_12)[6]);
        soal8.setText(getResources().getStringArray(R.array.pengukuran_bulan_12)[7]);
        soal9.setText(getResources().getStringArray(R.array.pengukuran_bulan_12)[8]);
        soal10.setText(getResources().getStringArray(R.array.pengukuran_bulan_12)[9]);

        no1.setChecked(true);
        no2.setChecked(true);
        no3.setChecked(true);
        no4.setChecked(true);
        no5.setChecked(true);
        no6.setChecked(true);
        no7.setChecked(true);
        no8.setChecked(true);
        no9.setChecked(true);
        no10.setChecked(true);
    }

    @OnClick(R.id.btn_kpsp_save)
    public void send(){
        checkAnswer();
        sendData();
    }

    private void sendData() {
        /**
         * Create Firebase references
         */
        Firebase userListsRef = new Firebase(Const.FIREBASE_URL_BAYI);
        final Firebase firebaseRef = new Firebase(Const.FIREBASE_URL);

        Firebase newListRef = userListsRef.push();

            /* Save listsRef.push() to maintain same random Id */
        final String listId = newListRef.getKey();

            /* HashMap for data to update */
        HashMap<String, Object> updateShoppingListData = new HashMap<>();

        /**
         * Set raw version of date to the ServerValue.TIMESTAMP value and save into
         * timestampCreatedMap
         */
        HashMap<String, Object> timestampCreated = new HashMap<>();
        timestampCreated.put(Const.FIREBASE_PROPERTY_TIMESTAMP, ServerValue.TIMESTAMP);

            /* Build the Bayi list */
        KategoriSatu newBayiList = new KategoriSatu(ans1,ans2,ans3,ans4,ans5,ans6,ans7,ans8,ans9,ans10, timestampCreated);

        HashMap<String, Object> kpspListMap = (HashMap<String, Object>)
                new ObjectMapper().convertValue(newBayiList, Map.class);

        Utils.insertDataSoalKpspBayi(null, mListId, Const.BULAN12,
                updateShoppingListData, "", kpspListMap);

        updateShoppingListData.put("/" + Const.FIREBASE_LOCATION_PENGUKURAN_MAPPINGS + "/" + mListId
                        + "/" + Const.BULAN12 + "/" + "kpsp",
                1);

            /* Do the update */
        firebaseRef.updateChildren(updateShoppingListData, new Firebase.CompletionListener() {
            @Override
            public void onComplete(FirebaseError firebaseError, Firebase firebase) {
                    /* Now that we have the timestamp, update the reversed timestamp */
//                    Utils.updateTimestampReversed(firebaseError, "AddList", listId,
//                            null, mEncodedEmail);
            }
        });

        /**
         * Close the dialog fragment when done
         */
        Intent intent = new Intent(getApplicationContext(), PengukuranActivity.class);
        startActivity(intent);
    }

    private void checkAnswer() {
        if(yes1.isChecked()) ans1 = true;
        else ans1 = false;

        if(yes2.isChecked()) ans2 = true;
        else ans2 = false;

        if(yes3.isChecked()) ans3 = true;
        else ans3 = false;

        if(yes4.isChecked()) ans4 = true;
        else ans4 = false;

        if(yes5.isChecked()) ans5 = true;
        else ans5 = false;

        if(yes6.isChecked()) ans6 = true;
        else ans6 = false;

        if(yes7.isChecked()) ans7 = true;
        else ans7 = false;

        if(yes8.isChecked()) ans8 = true;
        else ans8 = false;

        if(yes9.isChecked()) ans9 = true;
        else ans9 = false;

        if(yes10.isChecked()) ans10 = true;
        else ans10 = false;
    }
}
