package kominfo.android.pelatihan.pelatihanandroid.kelasPengolahanData;

/**
 * Created by dera on 30/05/2016.
 */
public class DayaLihat {
    int barisTerakhirKiri;
    int barisTerakhirKanan;
    public DayaLihat(int bTerakhirKiri, int bTerakhirKanan){
        barisTerakhirKiri = bTerakhirKiri;
        barisTerakhirKanan = bTerakhirKanan;
    }
    public String[] statusDayaLihat (){
        //method untuk mendapatkan status daya lihat dengan input baris terakhir yang terbaca oleh mata kiri dan mata kanan.Output ada 3 yaitu: status daya lihat kiri, status daya lihat kanan, intervensi
        String statusDayaLihatKiri="";
        String statusDayaLihatKanan="";
        String intervensi="";
        int i=0;
        //status daya lihat jika baris terakhir yang terbaca adalah 2 maka terjadi penyimpangan
        if (barisTerakhirKiri <= 2){
            statusDayaLihatKiri = "Kemungkinan mengalami gangguan";
            i++;
        }
        else{
            statusDayaLihatKiri = "Normal";
        }
        if (barisTerakhirKanan <= 2){
            statusDayaLihatKanan = "Kemungkinan mengalami gangguan";
            i++;
        }
        else{
            statusDayaLihatKanan = "Normal";
        }

        if (i>=1){
            intervensi = "Harus dibawa ke rumah sakit";
        }

        return new String[]{statusDayaLihatKiri,statusDayaLihatKanan,intervensi};
    }
}
