package kominfo.android.pelatihan.pelatihanandroid.activity.simulasi;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import kominfo.android.pelatihan.pelatihanandroid.R;
import kominfo.android.pelatihan.pelatihanandroid.activity.BaseActivity;

public class SimulasiActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simulasi);
    }
}
