package kominfo.android.pelatihan.pelatihanandroid.kelasPengolahanData;

/**
 * Created by dera on 30/05/2016.
 */
public class Pertumbuhan {
    //Deteksi Pertumbuhan
    boolean jenisKelamin;
    int umur;
    public Pertumbuhan(boolean jKelamin, int usia){
        jenisKelamin = jKelamin;
        umur = usia;
    }
    public String statusGizi (double tinggiBadan, double beratBadan){
        //method untuk mendapatkan status Gizi dengan input jenis kelamin, tinggi badan, dan berat badan
        String statusGizi = "";
        //status gizi perempuan
        if (jenisKelamin==false){
            if (tinggiBadan==55.0 ){
                if (beratBadan<=2.2){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>2.2)&&(beratBadan<=2.9)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>2.9)&&(beratBadan<=6.7)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==55.5 ){
                if (beratBadan<=2.3){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>2.3)&&(beratBadan<=3.0)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>3.0)&&(beratBadan<=6.9)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==56 ){
                if (beratBadan<=2.4){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>2.4)&&(beratBadan<=3.1)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>3.1)&&(beratBadan<=7.1)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==56.5 ){
                if (beratBadan<=2.5){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>2.5)&&(beratBadan<=3.3)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>3.3)&&(beratBadan<=7.3)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==57 ){
                if (beratBadan<=2.6){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>2.6)&&(beratBadan<=3.4)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>3.4)&&(beratBadan<=7.4)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==57.5 ){
                if (beratBadan<=2.7){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>2.7)&&(beratBadan<=3.5)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>3.5)&&(beratBadan<=7.6)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==58 ){
                if (beratBadan<=2.9){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>2.9)&&(beratBadan<=3.7)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>3.7)&&(beratBadan<=7.8)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==58.5 ){
                if (beratBadan<=3){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>3)&&(beratBadan<=3.8)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>3.8)&&(beratBadan<=7.9)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==59 ){
                if (beratBadan<=3.1){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>3.1)&&(beratBadan<=3.9)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>3.9)&&(beratBadan<=8.1)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==59.5 ){
                if (beratBadan<=3.2){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>3.2)&&(beratBadan<=4)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>4)&&(beratBadan<=8.2)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==60 ){
                if (beratBadan<=3.3){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>3.3)&&(beratBadan<=4.2)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>4.2)&&(beratBadan<=8.4)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==60.5 ){
                if (beratBadan<=3.4){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>3.4)&&(beratBadan<=4.3)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>4.3)&&(beratBadan<=8.6)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==61 ){
                if (beratBadan<=3.5){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>3.5)&&(beratBadan<=4.4)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>4.4)&&(beratBadan<=8.7)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==61.5 ){
                if (beratBadan<=3.6){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>3.6)&&(beratBadan<=4.5)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>4.5)&&(beratBadan<=8.9)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==62 ){
                if (beratBadan<=3.8){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>3.8)&&(beratBadan<=4.7)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>4.7)&&(beratBadan<=9)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==62.5 ){
                if (beratBadan<=3.9){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>3.9)&&(beratBadan<=4.8)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>4.8)&&(beratBadan<=9.2)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==63 ){
                if (beratBadan<=4){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>4)&&(beratBadan<=4.9)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>4.9)&&(beratBadan<=9.3)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==63.5 ){
                if (beratBadan<=4.1){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>4.1)&&(beratBadan<=5)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>5)&&(beratBadan<=9.4)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==64 ){
                if (beratBadan<=4.2){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>4.2)&&(beratBadan<=5.1)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>5.1)&&(beratBadan<=9.6)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==64.5 ){
                if (beratBadan<=4.3){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>4.3)&&(beratBadan<=5.3)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>5.3)&&(beratBadan<=9.7)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==65 ){
                if (beratBadan<=4.4){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>4.4)&&(beratBadan<=5.4)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>5.4)&&(beratBadan<=9.8)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==65.5 ){
                if (beratBadan<=4.5){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>4.5)&&(beratBadan<=5.5)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>5.5)&&(beratBadan<=10)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==66 ){
                if (beratBadan<=4.6){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>4.6)&&(beratBadan<=5.6)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>5.6)&&(beratBadan<=10.1)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==66.5 ){
                if (beratBadan<=4.7){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>4.7)&&(beratBadan<=5.7)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>5.7)&&(beratBadan<=10.2)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==67 ){
                if (beratBadan<=4.9){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>4.9)&&(beratBadan<=5.8)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>5.8)&&(beratBadan<=10.4)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==67.5 ){
                if (beratBadan<=5){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>5)&&(beratBadan<=6)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>6)&&(beratBadan<=10.5)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==68 ){
                if (beratBadan<=5.1){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>5.1)&&(beratBadan<=6.1)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>6.1)&&(beratBadan<=10.6)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==68.5 ){
                if (beratBadan<=5.2){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>5.2)&&(beratBadan<=6.2)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>6.2)&&(beratBadan<=10.7)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==69 ){
                if (beratBadan<=5.3){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>5.3)&&(beratBadan<=6.3)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>6.3)&&(beratBadan<=10.9)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==69.5 ){
                if (beratBadan<=5.4){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>5.4)&&(beratBadan<=6.4)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>6.4)&&(beratBadan<=11)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==70 ){
                if (beratBadan<=5.5){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>5.5)&&(beratBadan<=6.5)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>6.5)&&(beratBadan<=11.1)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==70.5 ){
                if (beratBadan<=5.6){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>5.6)&&(beratBadan<=6.6)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>6.6)&&(beratBadan<=11.2)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==71 ){
                if (beratBadan<=5.7){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>5.7)&&(beratBadan<=6.7)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>6.7)&&(beratBadan<=11.4)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==71.5 ){
                if (beratBadan<=5.8){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>5.8)&&(beratBadan<=6.8)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>6.8)&&(beratBadan<=11.5)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==72 ){
                if (beratBadan<=5.9){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>5.9)&&(beratBadan<=7)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>7)&&(beratBadan<=11.6)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==72.5 ){
                if (beratBadan<=6){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>6)&&(beratBadan<=7.1)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>7.1)&&(beratBadan<=11.7)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==73 ){
                if (beratBadan<=6.1){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>6.1)&&(beratBadan<=7.2)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>7.2)&&(beratBadan<=11.8)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==73.5 ){
                if (beratBadan<=6.3){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>6.3)&&(beratBadan<=7.3)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>7.3)&&(beratBadan<=11.9)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==74 ) {
                if (beratBadan <= 6.4) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 6.4) && (beratBadan <= 7.4)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 7.4) && (beratBadan <= 12.1)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==74.5 ){
                if (beratBadan<=6.5){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>6.5)&&(beratBadan<=7.5)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>7.5)&&(beratBadan<=12.2)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==75 ) {
                if (beratBadan <= 6.6) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 6.6) && (beratBadan <= 7.6)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 7.6) && (beratBadan <= 12.3)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==75.5 ){
                if (beratBadan<=6.7){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>6.7)&&(beratBadan<=7.7)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>7.7)&&(beratBadan<=12.4)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==76 ) {
                if (beratBadan <= 6.8) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 6.8) && (beratBadan <= 7.8)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 7.8) && (beratBadan <= 12.5)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==76.5 ){
                if (beratBadan<=6.9){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>6.9)&&(beratBadan<=7.9)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>7.9)&&(beratBadan<=12.6)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==77 ) {
                if (beratBadan <= 7) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 7) && (beratBadan <= 8)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 8) && (beratBadan <= 12.7)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==77.5 ){
                if (beratBadan<=7.1){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>7.1)&&(beratBadan<=8.1)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>8.1)&&(beratBadan<=12.8)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==78 ) {
                if (beratBadan <= 7.2) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 7.2) && (beratBadan <= 8.2)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 8.2) && (beratBadan <= 13)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==78.5 ){
                if (beratBadan<=7.3){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>7.3)&&(beratBadan<=8.3)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>8.3)&&(beratBadan<=13.1)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==79 ) {
                if (beratBadan <= 7.4) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 7.4) && (beratBadan <= 8.4)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 8.4) && (beratBadan <= 13.2)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==79.5 ){
                if (beratBadan<=7.5){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>7.5)&&(beratBadan<=8.5)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>8.5)&&(beratBadan<=13.3)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==80 ) {
                if (beratBadan <= 7.6) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 7.6) && (beratBadan <= 8.6)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 8.6) && (beratBadan <= 13.4)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==80.5 ){
                if (beratBadan<=7.7){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>7.7)&&(beratBadan<=8.7)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>8.7)&&(beratBadan<=13.5)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==81 ) {
                if (beratBadan <= 7.8) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 7.8) && (beratBadan <= 8.8)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 8.8) && (beratBadan <= 13.6)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==81.5 ){
                if (beratBadan<=7.9){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>7.9)&&(beratBadan<=8.9)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>8.9)&&(beratBadan<=13.8)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==82 ) {
                if (beratBadan <= 8) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 8) && (beratBadan <= 9)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 9) && (beratBadan <= 13.9)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==82.5 ){
                if (beratBadan<=8.1){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>8.1)&&(beratBadan<=9.1)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>9.1)&&(beratBadan<=14)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==83 ) {
                if (beratBadan <= 8.2) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 8.2) && (beratBadan <= 9.2)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 9.2) && (beratBadan <= 14.1)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==83.5 ){
                if (beratBadan<=8.2){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>8.2)&&(beratBadan<=9.3)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>9.3)&&(beratBadan<=14.2)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==84 ) {
                if (beratBadan <= 8.3) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 8.3) && (beratBadan <= 9.4)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 9.4) && (beratBadan <= 14.3)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==84.5 ){
                if (beratBadan<=8.4){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>8.4)&&(beratBadan<=9.5)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>9.5)&&(beratBadan<=14.4)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==85 ) {
                if (beratBadan <= 8.5) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 8.5) && (beratBadan <= 9.6)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 9.6) && (beratBadan <= 14.6)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==85.5 ){
                if (beratBadan<=8.6){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>8.6)&&(beratBadan<=9.7)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>9.7)&&(beratBadan<=14.7)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==86 ) {
                if (beratBadan <= 8.7) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 8.7) && (beratBadan <= 9.8)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 9.8) && (beratBadan <= 14.8)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==86.5 ){
                if (beratBadan<=8.8){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>8.8)&&(beratBadan<=9.9)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>9.9)&&(beratBadan<=14.9)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==87 ) {
                if (beratBadan <= 8.9) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 8.9) && (beratBadan <= 10)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 10) && (beratBadan <= 15.1)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==87.5 ){
                if (beratBadan<=9){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>9)&&(beratBadan<=10.1)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>10.1)&&(beratBadan<=15.2)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==88 ) {
                if (beratBadan <= 9.1) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 9.1) && (beratBadan <= 10.2)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 10.2) && (beratBadan <= 15.3)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==88.5 ){
                if (beratBadan<=9.2){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>9.2)&&(beratBadan<=10.3)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>10.3)&&(beratBadan<=15.4)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==89 ) {
                if (beratBadan <= 9.2) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 9.2) && (beratBadan <= 10.4)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 10.4) && (beratBadan <= 15.6)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==89.5 ){
                if (beratBadan<=9.3){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>9.3)&&(beratBadan<=10.5)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>10.5)&&(beratBadan<=15.7)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==90 ) {
                if (beratBadan <= 9.4) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 9.4) && (beratBadan <= 10.6)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 10.6) && (beratBadan <= 15.8)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==90.5 ){
                if (beratBadan<=9.5){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>9.5)&&(beratBadan<=10.6)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>10.6)&&(beratBadan<=15.9)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==91 ) {
                if (beratBadan <= 9.6) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 9.6) && (beratBadan <= 10.7)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 10.7) && (beratBadan <= 16.1)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==91.5 ){
                if (beratBadan<=9.7){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>9.7)&&(beratBadan<=10.8)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>10.8)&&(beratBadan<=16.2)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==92 ) {
                if (beratBadan <= 9.8) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 9.8) && (beratBadan <= 10.9)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 10.9) && (beratBadan <= 16.3)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==92.5 ){
                if (beratBadan<=9.8){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>9.8)&&(beratBadan<=11)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>11)&&(beratBadan<=16.5)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==93 ) {
                if (beratBadan <= 9.9) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 9.9) && (beratBadan <= 11.1)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 11.1) && (beratBadan <= 16.6)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==93.5 ){
                if (beratBadan<=10){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>10)&&(beratBadan<=11.2)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>11.2)&&(beratBadan<=16.7)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==94 ) {
                if (beratBadan <= 10.1) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 10.1) && (beratBadan <= 11.3)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 11.3) && (beratBadan <= 16.9)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==94.5 ){
                if (beratBadan<=10.2){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>10.2)&&(beratBadan<=11.4)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>11.4)&&(beratBadan<=17)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==95 ) {
                if (beratBadan <= 10.3) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 10.3) && (beratBadan <= 11.5)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 11.5) && (beratBadan <= 17.2)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==95.5 ){
                if (beratBadan<=10.4){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>10.4)&&(beratBadan<=11.6)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>11.6)&&(beratBadan<=17.3)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==96 ) {
                if (beratBadan <= 10.5) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 10.5) && (beratBadan <= 11.7)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 11.7) && (beratBadan <= 17.5)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==96.5 ){
                if (beratBadan <= 10.6) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 10.6) && (beratBadan <= 11.8)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 11.8) && (beratBadan <= 17.6)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==97 ) {
                if (beratBadan <= 10.6) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 10.6) && (beratBadan <= 11.9)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 11.9) && (beratBadan <= 17.8)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==97.5 ){
                if (beratBadan <= 10.7) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 10.7) && (beratBadan <= 12)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 12) && (beratBadan <= 17.9)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==98 ) {
                if (beratBadan <= 10.8) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 10.8) && (beratBadan <= 12.1)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 12.1) && (beratBadan <= 18.1)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==98.5 ){
                if (beratBadan <= 10.9) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 10.9) && (beratBadan <= 12.2)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 12.2) && (beratBadan <= 18.2)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==99 ) {
                if (beratBadan <= 11.0) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 11) && (beratBadan <= 12.3)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 12.3) && (beratBadan <= 18.4)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==99.5 ){
                if (beratBadan <= 11.1) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 11.1) && (beratBadan <= 12.4)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 12.4) && (beratBadan <= 18.5)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==100 ) {
                if (beratBadan <= 11.2) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 11.2) && (beratBadan <= 12.6)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 12.6) && (beratBadan <= 18.7)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==100.5 ){
                if (beratBadan <= 11.3) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 11.3) && (beratBadan <= 12.7)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 12.7) && (beratBadan <= 18.8)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==101 ) {
                if (beratBadan <= 11.4) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 11.4) && (beratBadan <= 12.8)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 12.8) && (beratBadan <= 19)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==101.5 ){
                if (beratBadan <= 11.5) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 11.5) && (beratBadan <= 12.9)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 12.9) && (beratBadan <= 19.1)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==102 ) {
                if (beratBadan <= 11.6) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 11.6) && (beratBadan <= 13)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 13) && (beratBadan <= 19.3)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==102.5 ){
                if (beratBadan <= 11.7) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 11.7) && (beratBadan <= 13.1)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 13.1) && (beratBadan <= 19.5)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==103 ) {
                if (beratBadan <= 11.8) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 11.8) && (beratBadan <= 13.2)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 13.2) && (beratBadan <= 19.6)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==103.5 ){
                if (beratBadan <= 11.9) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 11.9) && (beratBadan <= 13.3)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 13.3) && (beratBadan <= 19.8)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==104 ) {
                if (beratBadan <= 12) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 12) && (beratBadan <= 13.4)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 13.4) && (beratBadan <= 20)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==104.5 ){
                if (beratBadan <= 12.1) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 12.1) && (beratBadan <= 13.6)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 13.6) && (beratBadan <= 20.1)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==105 ) {
                if (beratBadan <= 12.2) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 12.2) && (beratBadan <= 13.7)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 13.7) && (beratBadan <= 20.3)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==105.5 ){
                if (beratBadan <= 12.3) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 12.3) && (beratBadan <= 13.8)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 13.8) && (beratBadan <= 20.5)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==106 ) {
                if (beratBadan <= 12.4) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 12.4) && (beratBadan <= 13.9)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 13.9) && (beratBadan <= 20.7)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==106.5 ){
                if (beratBadan <= 12.5) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 12.5) && (beratBadan <= 14)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 14) && (beratBadan <= 20.9)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==107 ) {
                if (beratBadan <= 12.6) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 12.6) && (beratBadan <= 14.2)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 14.2) && (beratBadan <= 21)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==107.5 ){
                if (beratBadan <= 12.7) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 12.7) && (beratBadan <= 14.3)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 14.3) && (beratBadan <= 21.2)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==108 ) {
                if (beratBadan <= 12.9) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 12.9) && (beratBadan <= 14.4)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 14.4) && (beratBadan <= 21.4)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==108.5 ){
                if (beratBadan <= 13) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 13) && (beratBadan <= 14.5)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 14.5) && (beratBadan <= 21.6)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==109 ) {
                if (beratBadan <= 13.1) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 13.1) && (beratBadan <= 14.7)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 14.7) && (beratBadan <= 21.8)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==109.5 ){
                if (beratBadan <= 13.2) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 13.2) && (beratBadan <= 14.8)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 14.8) && (beratBadan <= 22)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==110 ) {
                if (beratBadan <= 13.3) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 13.3) && (beratBadan <= 14.9)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 14.9) && (beratBadan <= 22.2)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==110.5 ){
                if (beratBadan <= 13.5) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 13.5) && (beratBadan <= 15.1)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 15.1) && (beratBadan <= 22.4)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==111 ) {
                if (beratBadan <= 13.6) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 13.6) && (beratBadan <= 15.2)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 15.2) && (beratBadan <= 22.6)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==111.5 ){
                if (beratBadan <= 13.7) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 13.7) && (beratBadan <= 15.4)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 15.4) && (beratBadan <= 22.8)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==112 ) {
                if (beratBadan <= 13.9) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 13.9) && (beratBadan <= 15.5)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 15.5) && (beratBadan <= 23)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==112.5 ){
                if (beratBadan <= 14) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 14) && (beratBadan <= 15.6)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 15.6) && (beratBadan <= 23.2)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==113 ) {
                if (beratBadan <= 14.1) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 14.1) && (beratBadan <= 15.8)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 15.8) && (beratBadan <= 23.4)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==113.5 ){
                if (beratBadan <= 14.3) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 14.3) && (beratBadan <= 15.9)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 15.9) && (beratBadan <= 23.6)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==114 ) {
                if (beratBadan <= 14.4) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 14.4) && (beratBadan <= 16.1)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 16.1) && (beratBadan <= 23.8)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==114.5 ){
                if (beratBadan <= 14.5) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 14.5) && (beratBadan <= 16.2)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 16.2) && (beratBadan <= 24.1)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==115 ) {
                if (beratBadan <= 14.7) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 14.7) && (beratBadan <= 16.4)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 16.4) && (beratBadan <= 24.3)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
        }








        //status gizi laki-laki
        else{
            if (tinggiBadan==55.0 ) {
                if (beratBadan <= 1.9) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 1.9) && (beratBadan <= 2.7)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 2.7) && (beratBadan <= 6.7)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==55.5 ){
                if (beratBadan<=2.1){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>2.1)&&(beratBadan<=2.8)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>2.8)&&(beratBadan<=6.9)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==56 ){
                if (beratBadan<=2.2){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>2.2)&&(beratBadan<=3.0)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>3.0)&&(beratBadan<=7.1)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==56.5 ){
                if (beratBadan<=2.3){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>2.3)&&(beratBadan<=3.1)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>3.1)&&(beratBadan<=7.3)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==57 ){
                if (beratBadan<=2.5){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>2.5)&&(beratBadan<=3.3)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>3.3)&&(beratBadan<=7.4)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==57.5 ){
                if (beratBadan<=2.6){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>2.6)&&(beratBadan<=3.4)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>3.4)&&(beratBadan<=7.6)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==58 ){
                if (beratBadan<=2.7){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>2.7)&&(beratBadan<=3.6)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>3.6)&&(beratBadan<=7.8)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==58.5 ){
                if (beratBadan<=2.9){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>2.9)&&(beratBadan<=3.7)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>3.7)&&(beratBadan<=7.9)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==59 ){
                if (beratBadan<=3){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>3)&&(beratBadan<=3.9)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>3.9)&&(beratBadan<=8.1)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==59.5 ){
                if (beratBadan<=3.1){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>3.1)&&(beratBadan<=4)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>4)&&(beratBadan<=8.2)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==60 ){
                if (beratBadan<=3.3){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>3.3)&&(beratBadan<=4.2)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>4.2)&&(beratBadan<=8.4)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==60.5 ){
                if (beratBadan<=3.4){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>3.4)&&(beratBadan<=4.3)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>4.3)&&(beratBadan<=8.6)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==61 ){
                if (beratBadan<=2.6){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>2.6)&&(beratBadan<=4.4)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>4.4)&&(beratBadan<=8.7)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==61.5 ){
                if (beratBadan<=3.7){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>3.7)&&(beratBadan<=4.4)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>4.4)&&(beratBadan<=8.9)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==62 ){
                if (beratBadan<=3.8){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>3.8)&&(beratBadan<=4.7)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>4.7)&&(beratBadan<=9)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==62.5 ){
                if (beratBadan<=3.9){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>3.9)&&(beratBadan<=4.8)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>4.8)&&(beratBadan<=9.2)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==63 ){
                if (beratBadan<=4){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>4)&&(beratBadan<=5)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>5)&&(beratBadan<=9.3)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==63.5 ){
                if (beratBadan<=4.2){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>4.2)&&(beratBadan<=5.1)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>5.1)&&(beratBadan<=9.5)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==64 ){
                if (beratBadan<=4.3){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>4.3)&&(beratBadan<=5.2)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>5.2)&&(beratBadan<=9.6)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==64.5 ){
                if (beratBadan<=4.4){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>4.4)&&(beratBadan<=5.4)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>5.4)&&(beratBadan<=9.8)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==65 ){
                if (beratBadan<=4.5){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>4.5)&&(beratBadan<=5.5)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>5.5)&&(beratBadan<=9.9)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==65.5 ){
                if (beratBadan<=4.6){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>4.6)&&(beratBadan<=5.6)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>5.6)&&(beratBadan<=10.1)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==66 ){
                if (beratBadan<=4.8){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>4.8)&&(beratBadan<=5.7)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>5.7)&&(beratBadan<=10.2)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==66.5 ){
                if (beratBadan<=4.9){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>4.9)&&(beratBadan<=5.9)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>5.9)&&(beratBadan<=10.4)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==67 ){
                if (beratBadan<=5){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>5)&&(beratBadan<=6)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>6)&&(beratBadan<=10.5)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==67.5 ){
                if (beratBadan<=5.1){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>5.1)&&(beratBadan<=6.1)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>6.1)&&(beratBadan<=10.7)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==68 ){
                if (beratBadan<=5.2){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>5.2)&&(beratBadan<=6.2)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>6.2)&&(beratBadan<=10.8)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==68.5 ){
                if (beratBadan<=5.4){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>5.4)&&(beratBadan<=6.3)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>6.3)&&(beratBadan<=10.9)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==69 ){
                if (beratBadan<=5.5){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>5.5)&&(beratBadan<=6.5)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>6.5)&&(beratBadan<=11.1)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==69.5 ){
                if (beratBadan<=5.6){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>5.6)&&(beratBadan<=6.6)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>6.6)&&(beratBadan<=11.2)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==70 ){
                if (beratBadan<=5.7){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>5.7)&&(beratBadan<=6.7)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>6.7)&&(beratBadan<=11.4)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==70.5 ){
                if (beratBadan<=5.8){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>5.8)&&(beratBadan<=6.8)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>6.8)&&(beratBadan<=11.5)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==71 ){
                if (beratBadan<=5.9){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>5.9)&&(beratBadan<=6.9)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>6.9)&&(beratBadan<=11.6)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==71.5 ){
                if (beratBadan<=6){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>6)&&(beratBadan<=7)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>7)&&(beratBadan<=11.8)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==72 ){
                if (beratBadan<=6.2){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>6.2)&&(beratBadan<=7.1)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>7.1)&&(beratBadan<=11.9)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==72.5 ){
                if (beratBadan<=6.3){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>6.3)&&(beratBadan<=7.3)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>7.3)&&(beratBadan<=12)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==73 ){
                if (beratBadan<=6.4){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>6.4)&&(beratBadan<=7.4)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>7.4)&&(beratBadan<=12.1)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==73.5 ){
                if (beratBadan<=6.5){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>6.5)&&(beratBadan<=7.5)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>7.5)&&(beratBadan<=12.3)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==74 ) {
                if (beratBadan <= 6.6) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 6.6) && (beratBadan <= 7.6)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 7.6) && (beratBadan <= 12.4)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==74.5 ){
                if (beratBadan<=6.7){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>6.7)&&(beratBadan<=7.7)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>7.7)&&(beratBadan<=12.5)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==75 ) {
                if (beratBadan <= 6.8) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 6.8) && (beratBadan <= 7.8)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 7.8) && (beratBadan <= 12.7)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==75.5 ){
                if (beratBadan<=6.9){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>6.9)&&(beratBadan<=7.9)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>7.9)&&(beratBadan<=12.8)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==76 ) {
                if (beratBadan <= 7) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 7) && (beratBadan <= 8)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 8) && (beratBadan <= 12.9)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==76.5 ){
                if (beratBadan<=7.1){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>7.1)&&(beratBadan<=8.1)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>8.1)&&(beratBadan<=13)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==77 ) {
                if (beratBadan <= 7.2) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 7.2) && (beratBadan <= 8.2)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 8.2) && (beratBadan <= 13.2)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==77.5 ){
                if (beratBadan<=7.3){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>7.3)&&(beratBadan<=8.3)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>8.3)&&(beratBadan<=13.3)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==78 ) {
                if (beratBadan <= 7.4) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 7.4) && (beratBadan <= 8.4)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 8.4) && (beratBadan <= 13.4)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==78.5 ){
                if (beratBadan<=7.5){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>7.5)&&(beratBadan<=8.5)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>8.5)&&(beratBadan<=13.5)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==79 ) {
                if (beratBadan <= 7.6) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 7.6) && (beratBadan <= 8.6)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 8.6) && (beratBadan <= 13.6)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==79.5 ){
                if (beratBadan<=7.7){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>7.7)&&(beratBadan<=8.7)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>8.7)&&(beratBadan<=13.8)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==80 ) {
                if (beratBadan <= 7.8) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 7.8) && (beratBadan <= 8.8)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 8.8) && (beratBadan <= 13.9)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==80.5 ){
                if (beratBadan<=7.9){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>7.9)&&(beratBadan<=8.9)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>8.9)&&(beratBadan<=14)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==81 ) {
                if (beratBadan <= 8) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 8) && (beratBadan <= 9)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 9) && (beratBadan <= 14.1)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==81.5 ){
                if (beratBadan<=8.1){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>8.1)&&(beratBadan<=9.1)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>9.1)&&(beratBadan<=14.2)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==82 ) {
                if (beratBadan <= 8.2) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 8.2) && (beratBadan <= 9.2)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 9.2) && (beratBadan <= 14.3)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==82.5 ){
                if (beratBadan<=8.3){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>8.3)&&(beratBadan<=9.3)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>9.3)&&(beratBadan<=14.5)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==83 ) {
                if (beratBadan <= 8.4) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 8.4) && (beratBadan <= 9.4)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 9.4) && (beratBadan <= 14.6)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==83.5 ){
                if (beratBadan<=8.5){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>8.5)&&(beratBadan<=9.5)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>9.5)&&(beratBadan<=14.7)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==84 ) {
                if (beratBadan <= 8.6) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 8.6) && (beratBadan <= 9.6)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 9.6) && (beratBadan <= 14.8)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==84.5 ){
                if (beratBadan<=8.7){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>8.7)&&(beratBadan<=9.7)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>9.7)&&(beratBadan<=14.9)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==85 ) {
                if (beratBadan <= 8.8) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 8.8) && (beratBadan <= 9.8)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 9.8) && (beratBadan <= 15)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==85.5 ){
                if (beratBadan<=8.8){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>8.8)&&(beratBadan<=9.9)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>9.9)&&(beratBadan<=15.1)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==86 ) {
                if (beratBadan <= 8.9) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 8.9) && (beratBadan <= 10)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 10) && (beratBadan <= 15.3)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==86.5 ){
                if (beratBadan<=9){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>9)&&(beratBadan<=10.1)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>10.1)&&(beratBadan<=15.4)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==87 ) {
                if (beratBadan <= 9.1) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 9.1) && (beratBadan <= 10.2)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 10.2) && (beratBadan <= 15.5)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==87.5 ){
                if (beratBadan<=9.2){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>9.2)&&(beratBadan<=10.3)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>10.3)&&(beratBadan<=15.6)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==88 ) {
                if (beratBadan <= 9.3) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 9.3) && (beratBadan <= 10.4)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 10.4) && (beratBadan <= 15.7)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==88.5 ){
                if (beratBadan<=9.4){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>9.4)&&(beratBadan<=10.5)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>10.5)&&(beratBadan<=15.8)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==89 ) {
                if (beratBadan <= 9.5) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 9.5) && (beratBadan <= 10.6)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 10.6) && (beratBadan <= 16)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==89.5 ){
                if (beratBadan<=9.6){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>9.6)&&(beratBadan<=10.7)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>10.7)&&(beratBadan<=16.1)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==90 ) {
                if (beratBadan <= 9.7) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 9.7) && (beratBadan <= 10.8)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 10.8) && (beratBadan <= 16.2)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==90.5 ){
                if (beratBadan<=9.8){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>9.8)&&(beratBadan<=10.9)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>10.9)&&(beratBadan<=16.3)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==91 ) {
                if (beratBadan <= 9.8) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 9.8) && (beratBadan <= 11)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 11) && (beratBadan <= 16.4)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==91.5 ){
                if (beratBadan<=9.9){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>9.9)&&(beratBadan<=11.1)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>11.1)&&(beratBadan<=16.5)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==92 ) {
                if (beratBadan <= 10) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 10) && (beratBadan <= 11.2)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 11.2) && (beratBadan <= 16.7)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==92.5 ){
                if (beratBadan<=10.1){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>10.1)&&(beratBadan<=11.3)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>11.3)&&(beratBadan<=16.8)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==93 ) {
                if (beratBadan <= 10.2) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 10.2) && (beratBadan <= 11.4)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 11.4) && (beratBadan <= 16.9)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==93.5 ){
                if (beratBadan<=10.3){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>10.3)&&(beratBadan<=11.5)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>11.5)&&(beratBadan<=17)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==94 ) {
                if (beratBadan <= 10.4) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 10.4) && (beratBadan <= 11.6)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 11.6) && (beratBadan <= 17.2)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==94.5 ){
                if (beratBadan<=10.5){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>10.5)&&(beratBadan<=11.7)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>11.7)&&(beratBadan<=17.3)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==95 ) {
                if (beratBadan <= 10.6) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 10.6) && (beratBadan <= 11.8)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 11.8) && (beratBadan <= 17.4)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==95.5 ){
                if (beratBadan<=10.7){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>10.7)&&(beratBadan<=11.9)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>11.9)&&(beratBadan<=17.5)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==96 ) {
                if (beratBadan <= 10.8) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 10.8) && (beratBadan <= 12)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 12) && (beratBadan <= 17.7)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==96.5 ){
                if (beratBadan<=10.9){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>10.9)&&(beratBadan<=12.1)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>12.1)&&(beratBadan<=17.8)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==97 ) {
                if (beratBadan <= 10.9) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 10.9) && (beratBadan <= 12.3)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 12.3) && (beratBadan <= 17.9)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==97.5 ){
                if (beratBadan<=11){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>11)&&(beratBadan<=12.4)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>12.4)&&(beratBadan<=18.1)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==98 ) {
                if (beratBadan <= 11.1) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 11.1) && (beratBadan <= 12.5)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 12.5) && (beratBadan <= 18.2)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==98.5 ){
                if (beratBadan<=11.2){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>11.2)&&(beratBadan<=12.6)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>12.6)&&(beratBadan<=18.4)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==99 ) {
                if (beratBadan <= 11.3) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 11.3) && (beratBadan <= 12.7)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 12.7) && (beratBadan <= 18.5)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==99.5 ){
                if (beratBadan<=11.4){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>11.4)&&(beratBadan<=12.8)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>12.8)&&(beratBadan<=18.6)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==100 ) {
                if (beratBadan <= 11.5) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 11.5) && (beratBadan <= 12.9)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 12.9) && (beratBadan <= 18.8)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==100.5 ){
                if (beratBadan<=11.6){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>11.6)&&(beratBadan<=13)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>13)&&(beratBadan<=18.9)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==101 ) {
                if (beratBadan <= 11.7) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 11.7) && (beratBadan <= 13.1)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 13.1) && (beratBadan <= 19.1)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==101.5 ){
                if (beratBadan<=11.8){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>11.8)&&(beratBadan<=13.2)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>13.2)&&(beratBadan<=19.2)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==102 ) {
                if (beratBadan <= 11.9) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 11.9) && (beratBadan <= 13.3)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 13.3) && (beratBadan <= 19.4)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==102.5 ){
                if (beratBadan<=12){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>12)&&(beratBadan<=13.5)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>13.5)&&(beratBadan<=19.5)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==103 ) {
                if (beratBadan <= 12.1) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 12.1) && (beratBadan <= 13.6)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 13.6) && (beratBadan <= 19.7)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==103.5 ){
                if (beratBadan<=12.2){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>12.2)&&(beratBadan<=13.7)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>13.7)&&(beratBadan<=19.9)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==104 ) {
                if (beratBadan <= 12.3) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 12.3) && (beratBadan <= 13.8)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 13.8) && (beratBadan <= 20)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==104.5 ){
                if (beratBadan<=12.5){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>12.5)&&(beratBadan<=13.9)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>13.9)&&(beratBadan<=20.2)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==105 ) {
                if (beratBadan <= 12.6) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 12.6) && (beratBadan <= 14.1)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 14.1) && (beratBadan <= 20.4)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==105.5 ){
                if (beratBadan<=12.7){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>12.7)&&(beratBadan<=14.2)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>14.2)&&(beratBadan<=20.5)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==106 ) {
                if (beratBadan <= 12.8) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 12.8) && (beratBadan <= 14.3)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 14.3) && (beratBadan <= 20.7)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==106.5 ){
                if (beratBadan<=12.9){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>12.9)&&(beratBadan<=14.4)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>14.4)&&(beratBadan<=20.9)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==107 ) {
                if (beratBadan <= 13) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 13) && (beratBadan <= 14.6)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 14.6) && (beratBadan <= 21.1)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==107.5 ){
                if (beratBadan<=13.1){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>13.1)&&(beratBadan<=14.7)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>14.7)&&(beratBadan<=21.3)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==108 ) {
                if (beratBadan <= 13.3) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 13.3) && (beratBadan <= 14.8)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 14.8) && (beratBadan <= 21.4)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==108.5 ){
                if (beratBadan<=13.4){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>13.4)&&(beratBadan<=14.9)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>14.9)&&(beratBadan<=21.6)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==109 ) {
                if (beratBadan <= 13.5) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 13.5) && (beratBadan <= 15.1)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 15.1) && (beratBadan <= 21.8)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==109.5 ){
                if (beratBadan<=13.6){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>13.6)&&(beratBadan<=15.2)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>15.2)&&(beratBadan<=22)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==110 ) {
                if (beratBadan <= 13.7) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 13.7) && (beratBadan <= 15.3)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 15.3) && (beratBadan <= 22.2)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==110.5 ){
                if (beratBadan<=13.9){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>13.9)&&(beratBadan<=15.5)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>15.5)&&(beratBadan<=22.4)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==111 ) {
                if (beratBadan <= 14) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 14) && (beratBadan <= 15.6)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 15.6) && (beratBadan <= 22.6)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==111.5 ){
                if (beratBadan<=14.1){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>14.1)&&(beratBadan<=15.8)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>15.8)&&(beratBadan<=22.8)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==112 ) {
                if (beratBadan <= 14.3) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 14.3) && (beratBadan <= 15.9)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 15.9) && (beratBadan <= 23.1)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==112.5 ){
                if (beratBadan<=14.4){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>14.4)&&(beratBadan<=16)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>16)&&(beratBadan<=23.3)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==113 ) {
                if (beratBadan <= 14.5) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 14.5) && (beratBadan <= 16.2)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 16.2) && (beratBadan <= 23.5)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==113.5 ){
                if (beratBadan<=14.7){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>14.7)&&(beratBadan<=16.3)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>16.3)&&(beratBadan<=23.7)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==114 ) {
                if (beratBadan <= 14.8) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 14.8) && (beratBadan <= 16.5)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 16.5) && (beratBadan <= 24)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==114.5 ){
                if (beratBadan<=14.9){
                    statusGizi = "Kurus Sekali";
                }
                else if ((beratBadan>14.9)&&(beratBadan<=16.6)){
                    statusGizi = "Kurus";
                }
                else if ((beratBadan>16.6)&&(beratBadan<=24.2)){
                    statusGizi = "Normal";
                }
                else{
                    statusGizi = "Gemuk";
                }
            }
            if (tinggiBadan==115 ) {
                if (beratBadan <= 15.1) {
                    statusGizi = "Kurus Sekali";
                } else if ((beratBadan > 15.1) && (beratBadan <= 16.8)) {
                    statusGizi = "Kurus";
                } else if ((beratBadan > 16.8) && (beratBadan <= 24.4)) {
                    statusGizi = "Normal";
                } else {
                    statusGizi = "Gemuk";
                }
            }
        }
        return statusGizi;
    }

    public String[] statusLingkarKepala (double lingkarKepala){
        //method untuk mendapatkan status lingkar kepala dengan input jenis kelamin, umur, dan ukuran lingkar kepala.Output ada 2 yaitu status lingkar kepala dan intervensi
        String statusLingkarKepala = "";
        String intervensi="";
        //status lingkar kepala perempuan
        if (jenisKelamin==false){
            if (umur == 3) {
                if (lingkarKepala < 37.0) {
                    statusLingkarKepala = "Mikrosefal";
                    intervensi = "Harus dibawa ke rumah sakit";
                } else if ((lingkarKepala >= 37.0) && (lingkarKepala <= 42.0)) {
                    statusLingkarKepala = "Normal";
                } else if (lingkarKepala > 42.0) {
                    statusLingkarKepala = "Makrosefal";
                    intervensi = "Harus dibawa ke rumah sakit";
                }
            }
            else if (umur == 6) {
                if (lingkarKepala < 40.0) {
                    statusLingkarKepala = "Mikrosefal";
                    intervensi = "Harus dibawa ke rumah sakit";
                } else if ((lingkarKepala >= 40.0) && (lingkarKepala <= 45.5)) {
                    statusLingkarKepala = "Normal";
                } else if (lingkarKepala > 45.5) {
                    statusLingkarKepala = "Makrosefal";
                    intervensi = "Harus dibawa ke rumah sakit";
                }
            }
            else if (umur == 9) {
                if (lingkarKepala < 42.0) {
                    statusLingkarKepala = "Mikrosefal";
                    intervensi = "Harus dibawa ke rumah sakit";
                } else if ((lingkarKepala >= 42.0) && (lingkarKepala <= 47.0)) {
                    statusLingkarKepala = "Normal";
                } else if (lingkarKepala > 47.0) {
                    statusLingkarKepala = "Makrosefal";
                    intervensi = "Harus dibawa ke rumah sakit";
                }
            }
            else if (umur == 12) {
                if (lingkarKepala < 43.5) {
                    statusLingkarKepala = "Mikrosefal";
                    intervensi = "Harus dibawa ke rumah sakit";
                } else if ((lingkarKepala >= 43.5) && (lingkarKepala <= 48.0)) {
                    statusLingkarKepala = "Normal";
                } else if (lingkarKepala > 48.0) {
                    statusLingkarKepala = "Makrosefal";
                    intervensi = "Harus dibawa ke rumah sakit";
                }
            }
            else if (umur == 18) {
                if (lingkarKepala < 44.5) {
                    statusLingkarKepala = "Mikrosefal";
                    intervensi = "Harus dibawa ke rumah sakit";
                } else if ((lingkarKepala >= 44.5) && (lingkarKepala <= 50.0)) {
                    statusLingkarKepala = "Normal";
                } else if (lingkarKepala > 50.0) {
                    statusLingkarKepala = "Makrosefal";
                    intervensi = "Harus dibawa ke rumah sakit";
                }
            }
            else if (umur == 24) {
                if (lingkarKepala < 45.5) {
                    statusLingkarKepala = "Mikrosefal";
                    intervensi = "Harus dibawa ke rumah sakit";
                } else if ((lingkarKepala >= 45.5) && (lingkarKepala <= 51.0)) {
                    statusLingkarKepala = "Normal";
                } else if (lingkarKepala > 51.0) {
                    statusLingkarKepala = "Makrosefal";
                    intervensi = "Harus dibawa ke rumah sakit";
                }
            }
            else if (umur == 30) {
                if (lingkarKepala < 45.5) {
                    statusLingkarKepala = "Mikrosefal";
                    intervensi = "Harus dibawa ke rumah sakit";
                } else if ((lingkarKepala >= 45.5) && (lingkarKepala <= 51.0)) {
                    statusLingkarKepala = "Normal";
                } else if (lingkarKepala > 51.0) {
                    statusLingkarKepala = "Makrosefal";
                    intervensi = "Harus dibawa ke rumah sakit";
                }
            }
            else if (umur == 36) {
                if (lingkarKepala < 46.0) {
                    statusLingkarKepala = "Mikrosefal";
                    intervensi = "Harus dibawa ke rumah sakit";
                } else if ((lingkarKepala >= 46.0) && (lingkarKepala <= 51.5)) {
                    statusLingkarKepala = "Normal";
                } else if (lingkarKepala > 51.5) {
                    statusLingkarKepala = "Makrosefal";
                    intervensi = "Harus dibawa ke rumah sakit";
                }
            }
            else if (umur == 42) {
                if (lingkarKepala < 47.5) {
                    statusLingkarKepala = "Mikrosefal";
                    intervensi = "Harus dibawa ke rumah sakit";
                } else if ((lingkarKepala >= 47.5) && (lingkarKepala <= 53.0)) {
                    statusLingkarKepala = "Normal";
                } else if (lingkarKepala > 53.0) {
                    statusLingkarKepala = "Makrosefal";
                    intervensi = "Harus dibawa ke rumah sakit";
                }
            }
            else if (umur == 48) {
                if (lingkarKepala < 47.0) {
                    statusLingkarKepala = "Mikrosefal";
                    intervensi = "Harus dibawa ke rumah sakit";
                } else if ((lingkarKepala >= 47.0) && (lingkarKepala <= 53.0)) {
                    statusLingkarKepala = "Normal";
                } else if (lingkarKepala > 53.0) {
                    statusLingkarKepala = "Makrosefal";
                    intervensi = "Harus dibawa ke rumah sakit";
                }
            }
            else if (umur == 54) {
                if (lingkarKepala < 48.0) {
                    statusLingkarKepala = "Mikrosefal";
                    intervensi = "Harus dibawa ke rumah sakit";
                } else if ((lingkarKepala >= 48.0) && (lingkarKepala <= 53.0)) {
                    statusLingkarKepala = "Normal";
                } else if (lingkarKepala > 53.0) {
                    statusLingkarKepala = "Makrosefal";
                    intervensi = "Harus dibawa ke rumah sakit";
                }
            }

            else if (umur == 60) {
                if (lingkarKepala < 48.0) {
                    statusLingkarKepala = "Mikrosefal";
                    intervensi = "Harus dibawa ke rumah sakit";
                } else if ((lingkarKepala >= 48.0) && (lingkarKepala <= 53.0)) {
                    statusLingkarKepala = "Normal";
                } else if (lingkarKepala > 53.0) {
                    statusLingkarKepala = "Makrosefal";
                    intervensi = "Harus dibawa ke rumah sakit";
                }
            }
            else if (umur == 66) {
                if (lingkarKepala < 48.0) {
                    statusLingkarKepala = "Mikrosefal";
                    intervensi = "Harus dibawa ke rumah sakit";
                } else if ((lingkarKepala >= 48.0) && (lingkarKepala <= 53.0)) {
                    statusLingkarKepala = "Normal";
                } else if (lingkarKepala > 53.0) {
                    statusLingkarKepala = "Makrosefal";
                    intervensi = "Harus dibawa ke rumah sakit";
                }
            }
            else if (umur == 72) {
                if (lingkarKepala < 48.0) {
                    statusLingkarKepala = "Mikrosefal";
                    intervensi = "Harus dibawa ke rumah sakit";
                } else if ((lingkarKepala >= 48.0) && (lingkarKepala <= 53.0)) {
                    statusLingkarKepala = "Normal";
                } else if (lingkarKepala > 53.0) {
                    statusLingkarKepala = "Makrosefal";
                    intervensi = "Harus dibawa ke rumah sakit";
                }
            }

        }
        //status lingkar kepala laki-laki
        else{
            if (umur == 3) {
                if (lingkarKepala < 38.0) {
                    statusLingkarKepala = "Mikrosefal";
                    intervensi = "Harus dibawa ke rumah sakit";
                } else if ((lingkarKepala >= 38.0) && (lingkarKepala <= 43.0)) {
                    statusLingkarKepala = "Normal";
                } else if (lingkarKepala > 43.0) {
                    statusLingkarKepala = "Makrosefal";
                    intervensi = "Harus dibawa ke rumah sakit";
                }
            }
            else if (umur == 6) {
                if (lingkarKepala < 41.0) {
                    statusLingkarKepala = "Mikrosefal";
                    intervensi = "Harus dibawa ke rumah sakit";
                } else if ((lingkarKepala >= 41.0) && (lingkarKepala <= 46.0)) {
                    statusLingkarKepala = "Normal";
                } else if (lingkarKepala > 46.0) {
                    statusLingkarKepala = "Makrosefal";
                    intervensi = "Harus dibawa ke rumah sakit";
                }
            }
            else if (umur == 9) {
                if (lingkarKepala < 43.0) {
                    statusLingkarKepala = "Mikrosefal";
                    intervensi = "Harus dibawa ke rumah sakit";
                } else if ((lingkarKepala >= 43.0) && (lingkarKepala <= 48.0)) {
                    statusLingkarKepala = "Normal";
                } else if (lingkarKepala > 48.0) {
                    statusLingkarKepala = "Makrosefal";
                    intervensi = "Harus dibawa ke rumah sakit";
                }
            }
            else if (umur == 12) {
                if (lingkarKepala < 44.0) {
                    statusLingkarKepala = "Mikrosefal";
                    intervensi = "Harus dibawa ke rumah sakit";
                } else if ((lingkarKepala >= 44.0) && (lingkarKepala <= 49.5)) {
                    statusLingkarKepala = "Normal";
                } else if (lingkarKepala > 49.5) {
                    statusLingkarKepala = "Makrosefal";
                    intervensi = "Harus dibawa ke rumah sakit";
                }
            }
            else if (umur == 18) {
                if (lingkarKepala < 45.5) {
                    statusLingkarKepala = "Mikrosefal";
                    intervensi = "Harus dibawa ke rumah sakit";
                } else if ((lingkarKepala >= 45.5) && (lingkarKepala <= 51.0)) {
                    statusLingkarKepala = "Normal";
                } else if (lingkarKepala > 51.0) {
                    statusLingkarKepala = "Makrosefal";
                    intervensi = "Harus dibawa ke rumah sakit";
                }
            }
            else if (umur == 24) {
                if (lingkarKepala < 46.0) {
                    statusLingkarKepala = "Mikrosefal";
                    intervensi = "Harus dibawa ke rumah sakit";
                } else if ((lingkarKepala >= 46.0) && (lingkarKepala <= 52.0)) {
                    statusLingkarKepala = "Normal";
                } else if (lingkarKepala > 52.0) {
                    statusLingkarKepala = "Makrosefal";
                    intervensi = "Harus dibawa ke rumah sakit";
                }
            }
            else if (umur == 30) {
                if (lingkarKepala < 47.0) {
                    statusLingkarKepala = "Mikrosefal";
                    intervensi = "Harus dibawa ke rumah sakit";
                } else if ((lingkarKepala >= 47.0) && (lingkarKepala <= 52.5)) {
                    statusLingkarKepala = "Normal";
                } else if (lingkarKepala > 52.5) {
                    statusLingkarKepala = "Makrosefal";
                    intervensi = "Harus dibawa ke rumah sakit";
                }
            }
            else if (umur == 36) {
                if (lingkarKepala < 48.0) {
                    statusLingkarKepala = "Mikrosefal";
                    intervensi = "Harus dibawa ke rumah sakit";
                } else if ((lingkarKepala >= 48.0) && (lingkarKepala <= 53.0)) {
                    statusLingkarKepala = "Normal";
                } else if (lingkarKepala > 53.0) {
                    statusLingkarKepala = "Makrosefal";
                    intervensi = "Harus dibawa ke rumah sakit";
                }
            }
            else if (umur == 42) {
                if (lingkarKepala < 48.0) {
                    statusLingkarKepala = "Mikrosefal";
                    intervensi = "Harus dibawa ke rumah sakit";
                } else if ((lingkarKepala >= 48.0) && (lingkarKepala <= 53.5)) {
                    statusLingkarKepala = "Normal";
                } else if (lingkarKepala > 53.5) {
                    statusLingkarKepala = "Makrosefal";
                    intervensi = "Harus dibawa ke rumah sakit";
                }
            }
            else if (umur == 48) {
                if (lingkarKepala < 48.0) {
                    statusLingkarKepala = "Mikrosefal";
                    intervensi = "Harus dibawa ke rumah sakit";
                } else if ((lingkarKepala >= 48.0) && (lingkarKepala <= 53.5)) {
                    statusLingkarKepala = "Normal";
                } else if (lingkarKepala > 53.5) {
                    statusLingkarKepala = "Makrosefal";
                    intervensi = "Harus dibawa ke rumah sakit";
                }
            }
            else if (umur == 54) {
                if (lingkarKepala < 48.0) {
                    statusLingkarKepala = "Mikrosefal";
                    intervensi = "Harus dibawa ke rumah sakit";
                } else if ((lingkarKepala >= 48.0) && (lingkarKepala <= 53.5)) {
                    statusLingkarKepala = "Normal";
                } else if (lingkarKepala > 53.5) {
                    statusLingkarKepala = "Makrosefal";
                    intervensi = "Harus dibawa ke rumah sakit";
                }
            }
            else if (umur == 60) {
                if (lingkarKepala < 48.0) {
                    statusLingkarKepala = "Mikrosefal";
                    intervensi = "Harus dibawa ke rumah sakit";
                } else if ((lingkarKepala >= 48.0) && (lingkarKepala <= 54.0)) {
                    statusLingkarKepala = "Normal";
                } else if (lingkarKepala > 54.0) {
                    statusLingkarKepala = "Makrosefal";
                    intervensi = "Harus dibawa ke rumah sakit";
                }
            }
            else if (umur == 66) {
                if (lingkarKepala < 48.5) {
                    statusLingkarKepala = "Mikrosefal";
                    intervensi = "Harus dibawa ke rumah sakit";
                } else if ((lingkarKepala >= 48.5) && (lingkarKepala <= 54.0)) {
                    statusLingkarKepala = "Normal";
                } else if (lingkarKepala > 54.0) {
                    statusLingkarKepala = "Makrosefal";
                    intervensi = "Harus dibawa ke rumah sakit";
                }
            }
            else if (umur == 72) {
                if (lingkarKepala < 48.5) {
                    statusLingkarKepala = "Mikrosefal";
                    intervensi = "Harus dibawa ke rumah sakit";
                } else if ((lingkarKepala >= 48.5) && (lingkarKepala <= 54.0)) {
                    statusLingkarKepala = "Normal";
                } else if (lingkarKepala > 54.0) {
                    statusLingkarKepala = "Makrosefal";
                    intervensi = "Harus dibawa ke rumah sakit";
                }
            }
        }
        return new String[]{statusLingkarKepala,intervensi};
    }

}
