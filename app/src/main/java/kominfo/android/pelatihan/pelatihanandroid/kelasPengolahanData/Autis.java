package kominfo.android.pelatihan.pelatihanandroid.kelasPengolahanData;

/**
 * Created by dera on 30/05/2016.
 */
public class Autis {
    boolean a1,a2,a3,a4,a5,a6,a7,a8,a9,b1,b2,b3,b4,b5;
    public Autis(boolean c1,boolean c2,boolean c3,boolean c4,boolean c5,boolean c6,boolean c7,boolean c8,boolean c9,boolean d1,boolean d2,boolean d3,boolean d4,boolean d5){
        a1=c1;
        a2=c2;
        a3=c3;
        a4=c4;
        a5=c5;
        a6=c6;
        a7=c7;
        a8=c8;
        a9=c9;
        b1=d1;
        b2=d2;
        b3=d3;
        b4=d4;
        b5=d5;
    }
    public String[] statusAutis (){
        //method untuk mendapatkan status autis dengan Output ada 2 yaitu status Autis dan intervensi
        String statusAutis="";
        String intervensi ="";
        if ((a5 == false)||(a7 == false)||(b2 == false)||(b3 == false)||(b4 == false)){
            statusAutis = "Resiko tinggi menderita Autis";
            intervensi = "Harus dibawa ke rumah sakit dengan fasilitas kesehatan jiwa/tumbuh kembang anak";
        }
        else if ((a1 == false)||(a2 == false)||(a3 == false)||(a4 == false)||(a6 == false)||(a8 == false)||(a9 == false)||(b1 == false)||(b5 == false)){
            statusAutis = "Kemungkinan gangguan perkembangan lain";
            intervensi = "Harus dibawa ke rumah sakit dengan fasilitas kesehatan jiwa/tumbuh kembang anak";
        }
        else if((a7==false)||(b4==false)) {
            statusAutis = "Resiko rendah menderita Autis";
            intervensi = "Harus dibawa ke rumah sakit dengan fasilitas kesehatan jiwa/tumbuh kembang anak";
        }
        else{
            statusAutis = "Normal";
        }
        return new String[]{statusAutis,intervensi};
    }
}
