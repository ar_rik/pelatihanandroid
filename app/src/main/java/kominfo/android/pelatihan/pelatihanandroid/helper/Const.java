package kominfo.android.pelatihan.pelatihanandroid.helper;

import kominfo.android.pelatihan.pelatihanandroid.BuildConfig;

/**
 * Created by erdearik on 5/30/16.
 */
public class Const {
    /**
     * Constants related to locations in Firebase, such as the name of the node
     * where user lists are stored (ie "userLists")
     */
    public static final String FIREBASE_LOCATION_USERS = "users";
    public static final String FIREBASE_LOCATION_USER_LISTS = "userLists";
    public static final String FIREBASE_LOCATION_UID_MAPPINGS = "uidMappings";
    public static final String FIREBASE_LOCATION_OWNER_MAPPINGS ="ownerMappings" ;
    public static final String FIREBASE_LOCATION_BAYI = "dataBayi";
    public static final String FIREBASE_LOCATION_DATA_KPSP = "dataKpsp";
    public static final String FIREBASE_LOCATION_PENGUKURAN_MAPPINGS = "pengukuranMapppings";


    /**
     * Constants for Firebase URL
     */
    public static final String FIREBASE_URL = BuildConfig.UNIQUE_FIREBASE_ROOT_URL;
    public static final String FIREBASE_URL_USERS = FIREBASE_URL + "/" + FIREBASE_LOCATION_USERS;
    public static final String FIREBASE_URL_USER_LISTS = FIREBASE_URL + "/" + FIREBASE_LOCATION_USER_LISTS;
    public static final String FIREBASE_URL_BAYI = FIREBASE_URL + "/" + FIREBASE_LOCATION_BAYI;

    /**
     * Constants for bundles, extras and shared preferences key
     */
    public static final String KEY_LIST_NAME = "LIST_NAME";
    public static final String KEY_LAYOUT_RESOURCE = "LAYOUT_RESOURCE";
    public static final String KEY_LIST_ID = "LIST_ID";
    public static final String KEY_SIGNUP_EMAIL = "SIGNUP_EMAIL";
    public static final String KEY_LIST_ITEM_NAME = "ITEM_NAME";
    public static final String KEY_LIST_ITEM_ID = "LIST_ITEM_ID";
    public static final String KEY_PROVIDER = "PROVIDER";
    public static final String KEY_ENCODED_EMAIL = "ENCODED_EMAIL";
    public static final String KEY_LIST_OWNER = "LIST_OWNER";
    public static final String KEY_GOOGLE_EMAIL = "GOOGLE_EMAIL";
    public static final String KEY_PREF_SORT_ORDER_LISTS = "PERF_SORT_ORDER_LISTS";
    public static final String KEY_SHARED_WITH_USERS = "SHARED_WITH_USERS";
    public static final String PASSWORD_PROVIDER = "password";
    public static final String GOOGLE_PROVIDER = "google";
    public static final String PROVIDER_DATA_DISPLAY_NAME = "displayName";

    /**
     * Constants for Firebase object properties
     */
    public static final String FIREBASE_PROPERTY_TIMESTAMP_LAST_CHANGED = "timestampLastChanged";
    public static final String FIREBASE_PROPERTY_TIMESTAMP = "timestamp";
    public static final String FIREBASE_PROPERTY_EMAIL = "email";
    public static final String FIREBASE_PROPERTY_USER_HAS_LOGGED_IN_WITH_PASSWORD = "hasLoggedInWithPassword";
    public static final String FIREBASE_PROPERTY_TIMESTAMP_LAST_CHANGED_REVERSE = "timestampLastChangedReverse";

    /**
     * Constant for sorting
     */
    public static final String ORDER_BY_KEY = "orderByPushKey";
    public static final String ORDER_BY_OWNER_EMAIL = "orderByOwnerEmail";

    /**
     * Constant Jenis Kelamin
     */
    public static final String LAKI = "0";
    public static final String PEREMPUAN = "1";

    /**
     * Constant Nama Bulan
     */
    public static final String BULAN3 = "bulan3";
    public static final String BULAN6 = "bulan6";
    public static final String BULAN9 = "bulan9";
    public static final String BULAN12 = "bulan12";
    public static final String BULAN15 = "bulan15";
    public static final String BULAN18 = "bulan18";
    public static final String BULAN21 = "bulan21";
    public static final String BULAN24 = "bulan24";
    public static final String BULAN30 = "bulan30";
    public static final String BULAN36 = "bulan36";
    public static final String BULAN42 = "bulan42";
    public static final String BULAN48 = "bulan48";
    public static final String BULAN54 = "bulan54";
    public static final String BULAN60 = "bulan60";
    public static final String BULAN66 = "bulan66";
    public static final String BULAN72 = "bulan72";

}
