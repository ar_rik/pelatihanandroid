package kominfo.android.pelatihan.pelatihanandroid.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kominfo.android.pelatihan.pelatihanandroid.R;
import kominfo.android.pelatihan.pelatihanandroid.activity.soal.SoalBulanEnamActivity;
import kominfo.android.pelatihan.pelatihanandroid.app.EndPoint;
import kominfo.android.pelatihan.pelatihanandroid.app.MyApplication;

public class DataDetailBayiPlusActivity extends AppCompatActivity implements Validator.ValidationListener{
    @NotEmpty
    @Bind(R.id.edt_detail_bayi_tambah_bb)
    EditText bb;

    @NotEmpty
    @Bind(R.id.edt_detail_bayi_tambah_tb)
    EditText tb;

    @NotEmpty
    @Bind(R.id.umur)
    EditText umur;
    @NotEmpty
    @Bind(R.id.edt_detail_bayi_tambah_lp)
    EditText lp;
    @Bind(R.id.nama)
    TextView nama;
    @Bind(R.id.tanggal)
    TextView tanggal;
    private Validator validator;
    private ProgressDialog dia;
    private String TAG = DataDetailBayiPlusActivity.class.getSimpleName();
    private String namaa, lahir, id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pengukuran);
        setContentView(R.layout.activity_data_detail_bayi_plus);
        ButterKnife.bind(this);
        Intent i = getIntent();
        namaa = i.getStringExtra("nama");
        lahir = i.getStringExtra("lahir");
        id = i.getStringExtra("id");

        nama.setText(namaa);
        tanggal.setText(lahir);
        validator = new Validator(this);
        validator.setValidationListener(this);
    }

    @Override
    public void onValidationSucceeded() {
        saveData();
    }

    private void saveData() {
        String endPoint = EndPoint.BAYI_ID_ALL.replace("_ID_", id);
        StringRequest strReq = new StringRequest(Request.Method.POST,
                endPoint, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);
                    Log.e(TAG, "response (try): " + obj);
//                    Toast.makeText(DataBayiPlusActivity.this, "Data di tambahkan", Toast.LENGTH_SHORT).show();
                    // check for error
                    if (obj.getBoolean("error") == false) {
//                        dia.dismiss();
                        backto();
//                        Log.e(TAG, "response (try error): " + obj.getBoolean("error"));
                        JSONObject commentObj = obj.getJSONObject("message");
                        Toast.makeText(getApplicationContext(), "" + obj.getString("message"), Toast.LENGTH_LONG).show();
                    } else {
//                        dia.dismiss();
//                        Log.e(TAG, "response (try error true): " + obj.getBoolean("error"));
                        Toast.makeText(getApplicationContext(), "" + obj.getString("message"), Toast.LENGTH_LONG).show();
                    }
                    Toast.makeText(getApplicationContext(), "" + obj.getString("message"), Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    Log.e(TAG, "json parsing error(send): " + e.getMessage());
                    Toast.makeText(getApplicationContext(), "json parse error(send): " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error(error response): " + error.getMessage() + ", code: " + networkResponse);
                Toast.makeText(getApplicationContext(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("umur", umur.getText().toString());
                params.put("umur", nama.getText().toString());
                params.put("tanggal_pemeriksaan",new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
                params.put("bb",bb.getText().toString());
                params.put("bt",tb.getText().toString());
                params.put("lingkar_kepala",lp.getText().toString());

                Log.e(TAG, "Params: " + params.toString());

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map headers = new HashMap();
                headers.put("Authorization", "746be99d4ef39262e059f3afb7a9cb4f");
                headers.put("Content-Type","application/x-www-form-urlencoded");

                return headers;
            }
        };

        // disabling retry policy so that it won't make
        // multiple http calls
        int socketTimeout = 0;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        strReq.setRetryPolicy(policy);

        //Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq);
    }

    private void backto() {
//        Intent i = new Intent(getApplicationContext(), DataDetailBayiActivity.class);
//        i.putExtra("namabayi",namaa);
//        i.putExtra("lahir",lahir);
//        i.putExtra("idbayi",id);
//        startActivity(i);
        Intent i = new Intent(getApplicationContext(), SoalBulanEnamActivity.class);
        i.putExtra("namabayi",namaa);
        i.putExtra("lahir",lahir);
        i.putExtra("idbayi",id);
        startActivity(i);
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    @OnClick(R.id.btn_tambah_detail_bayi_can)
    public void can(){

    }

    @OnClick(R.id.btn_tambah_detail_bayi_save)
    public void save(){
        validator.validate();
    }
}
