package kominfo.android.pelatihan.pelatihanandroid.app;

/**
 * Created by erdearik on 3/8/16.
 */
public class EndPoint {
    public static final String BASE_URL_lama = "http://jerucommerce.com/get_all_students.php";
    public static final String BASE_URL_local = "http://192.168.42.146/pelatihanandroid/v1";
    public static final String BASE_URL_C9 = "https://api-android-sportbudds.c9users.io/v1";
    public static final String BASE_URL = "http://bgrow.republikmurah.com/v1";
    public static final String DATA_BAYI = BASE_URL + "/databayi";
    public static final String BAYI_ID_ALL = BASE_URL + "/databayi/_ID_";
    public static final String LOGIN = BASE_URL + "/login" ;
    public static final String REGISTER = BASE_URL + "/register";
}
