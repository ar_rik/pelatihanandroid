package kominfo.android.pelatihan.pelatihanandroid.activity.pengukuran;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import butterknife.ButterKnife;
import butterknife.OnClick;
import kominfo.android.pelatihan.pelatihanandroid.R;
import kominfo.android.pelatihan.pelatihanandroid.activity.BaseActivity;
import kominfo.android.pelatihan.pelatihanandroid.activity.soal.SoalBulanDelapanBelasActivity;
import kominfo.android.pelatihan.pelatihanandroid.activity.soal.SoalBulanDuaBelasActivity;
import kominfo.android.pelatihan.pelatihanandroid.activity.soal.SoalBulanDuaEmpatActivity;
import kominfo.android.pelatihan.pelatihanandroid.activity.soal.SoalBulanDuaSatuActivity;
import kominfo.android.pelatihan.pelatihanandroid.activity.soal.SoalBulanEmpatDelapanActivity;
import kominfo.android.pelatihan.pelatihanandroid.activity.soal.SoalBulanEmpatDuaActivity;
import kominfo.android.pelatihan.pelatihanandroid.activity.soal.SoalBulanEnamActivity;
import kominfo.android.pelatihan.pelatihanandroid.activity.soal.SoalBulanEnamPuluhActivity;
import kominfo.android.pelatihan.pelatihanandroid.activity.soal.SoalBulanLimaBelasActivity;
import kominfo.android.pelatihan.pelatihanandroid.activity.soal.SoalBulanLimaEmpatActivity;
import kominfo.android.pelatihan.pelatihanandroid.activity.soal.SoalBulanSembilanActivity;
import kominfo.android.pelatihan.pelatihanandroid.activity.soal.SoalBulanTigaActivity;
import kominfo.android.pelatihan.pelatihanandroid.activity.soal.SoalBulanTigaEnamActivity;
import kominfo.android.pelatihan.pelatihanandroid.activity.soal.SoalBulanTigaPuluhActivity;

import kominfo.android.pelatihan.pelatihanandroid.helper.Const;
import kominfo.android.pelatihan.pelatihanandroid.helper.Utils;

public class PengukuranActivity extends BaseActivity {
    private int umurBulan;
    private String umurBelumCukup = "Umur anak anda belum memenuhi untuk melakukan test ini";
    private String mListId;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pengukuran2);
        ButterKnife.bind(this);

        Intent intent = this.getIntent();
        mListId = intent.getStringExtra(Const.KEY_LIST_ID);

        String datee = Utils.getCurrentDate();
        umurBulan = Utils.countMonth("2012-09-01",datee);
    }

    @OnClick(R.id.btn_pengukuran3)
    public void ukur3(){
        if(umurBulan<3){
            Toast.makeText(PengukuranActivity.this, umurBelumCukup, Toast.LENGTH_SHORT).show();
        } else {
            Intent intent = new Intent(getApplicationContext(), SoalBulanTigaActivity.class);
            intent.putExtra(Const.KEY_LIST_ID, mListId);
            startActivity(intent);
        }
    }

    @OnClick(R.id.btn_pengukuran6)
    public void ukur6(){
        if(umurBulan<6){
            Toast.makeText(PengukuranActivity.this, umurBelumCukup, Toast.LENGTH_SHORT).show();
        } else {
            Intent intent = new Intent(getApplicationContext(), SoalBulanEnamActivity.class);
            intent.putExtra(Const.KEY_LIST_ID, mListId);
            startActivity(intent);
        }
    }

    @OnClick(R.id.btn_pengukuran9)
    public void ukur9(){
        if(umurBulan<9){
            Toast.makeText(PengukuranActivity.this, umurBelumCukup, Toast.LENGTH_SHORT).show();
        } else {
            Intent intent = new Intent(getApplicationContext(), SoalBulanSembilanActivity.class);
            intent.putExtra(Const.KEY_LIST_ID, mListId);
            startActivity(intent);
        }
    }

    @OnClick(R.id.btn_pengukuran12)
    public void ukur12(){
        if(umurBulan<3){
            Toast.makeText(PengukuranActivity.this, umurBelumCukup, Toast.LENGTH_SHORT).show();
        } else {
            Intent intent = new Intent(getApplicationContext(), SoalBulanDuaBelasActivity.class);
            intent.putExtra(Const.KEY_LIST_ID, mListId);
            startActivity(intent);
        }
    }

    @OnClick(R.id.btn_pengukuran15)
    public void ukur15(){
        if(umurBulan<15){
            Toast.makeText(PengukuranActivity.this, umurBelumCukup, Toast.LENGTH_SHORT).show();
        } else {
            Intent intent = new Intent(getApplicationContext(), SoalBulanLimaBelasActivity.class);
            intent.putExtra(Const.KEY_LIST_ID, mListId);
            startActivity(intent);
        }
    }

    @OnClick(R.id.btn_pengukuran18)
    public void ukur18(){
        if(umurBulan<18){
            Toast.makeText(PengukuranActivity.this, umurBelumCukup, Toast.LENGTH_SHORT).show();
        } else {
            Intent intent = new Intent(getApplicationContext(), SoalBulanDelapanBelasActivity.class);
            intent.putExtra(Const.KEY_LIST_ID, mListId);
            startActivity(intent);
        }
    }

    @OnClick(R.id.btn_pengukuran21)
    public void ukur21(){
        if(umurBulan<21){
            Toast.makeText(PengukuranActivity.this, umurBelumCukup, Toast.LENGTH_SHORT).show();
        } else {
            Intent intent = new Intent(getApplicationContext(), SoalBulanDuaSatuActivity.class);
            intent.putExtra(Const.KEY_LIST_ID, mListId);
            startActivity(intent);
        }
    }

    @OnClick(R.id.btn_pengukuran24)
    public void ukur24(){
        if(umurBulan<24){
            Toast.makeText(PengukuranActivity.this, umurBelumCukup, Toast.LENGTH_SHORT).show();
        } else {
            Intent intent = new Intent(getApplicationContext(), SoalBulanDuaEmpatActivity.class);
            intent.putExtra(Const.KEY_LIST_ID, mListId);
            startActivity(intent);
        }
    }

    @OnClick(R.id.btn_pengukuran30)
    public void ukur30(){
        if(umurBulan<30){
            Toast.makeText(PengukuranActivity.this, umurBelumCukup, Toast.LENGTH_SHORT).show();
        } else {
            Intent intent = new Intent(getApplicationContext(), SoalBulanTigaPuluhActivity.class);
            intent.putExtra(Const.KEY_LIST_ID, mListId);
            startActivity(intent);
        }
    }

    @OnClick(R.id.btn_pengukuran36)
    public void ukur36(){
        if(umurBulan<36){
            Toast.makeText(PengukuranActivity.this, umurBelumCukup, Toast.LENGTH_SHORT).show();
        } else {
            Intent intent = new Intent(getApplicationContext(), SoalBulanTigaEnamActivity.class);
            intent.putExtra(Const.KEY_LIST_ID, mListId);
            startActivity(intent);
        }
    }

    @OnClick(R.id.btn_pengukuran42)
    public void ukur42(){
        if(umurBulan<42){
            Toast.makeText(PengukuranActivity.this, umurBelumCukup, Toast.LENGTH_SHORT).show();
        } else {
            Intent intent = new Intent(getApplicationContext(), SoalBulanEmpatDuaActivity.class);
            intent.putExtra(Const.KEY_LIST_ID, mListId);
            startActivity(intent);
        }
    }

    @OnClick(R.id.btn_pengukuran48)
    public void ukur48(){
        if(umurBulan<48){
            Toast.makeText(PengukuranActivity.this, umurBelumCukup, Toast.LENGTH_SHORT).show();
        } else {
            Intent intent = new Intent(getApplicationContext(), SoalBulanEmpatDelapanActivity.class);
            intent.putExtra(Const.KEY_LIST_ID, mListId);
            startActivity(intent);
        }
    }

    @OnClick(R.id.btn_pengukuran54)
    public void ukur54(){
        if(umurBulan<54){
            Toast.makeText(PengukuranActivity.this, umurBelumCukup, Toast.LENGTH_SHORT).show();
        } else {
            Intent intent = new Intent(getApplicationContext(), SoalBulanLimaEmpatActivity.class);
            intent.putExtra(Const.KEY_LIST_ID, mListId);
            startActivity(intent);
        }
    }

    @OnClick(R.id.btn_pengukuran60)
    public void ukur60(){
        if(umurBulan<60){
            Toast.makeText(PengukuranActivity.this, umurBelumCukup, Toast.LENGTH_SHORT).show();
        } else {
            Intent intent = new Intent(getApplicationContext(), SoalBulanEnamPuluhActivity.class);
            intent.putExtra(Const.KEY_LIST_ID, mListId);
            startActivity(intent);
        }
    }
}
