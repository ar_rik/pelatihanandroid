package kominfo.android.pelatihan.pelatihanandroid.model;

/**
 * Created by erdearik on 5/2/16.
 */
public class DataDetailBayi {
    private String id, umur, tanggal_pemeriksaan, bb, tb, lingkar_kepala;

    public DataDetailBayi() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUmur() {
        return umur;
    }

    public void setUmur(String umur) {
        this.umur = umur;
    }

    public DataDetailBayi(String id, String umur, String tanggal_pemeriksaan, String bb, String tb, String lingkar_kepala) {
        this.id = id;
        this.umur = umur;
        this.tanggal_pemeriksaan = tanggal_pemeriksaan;
        this.bb = bb;
        this.tb = tb;
        this.lingkar_kepala = lingkar_kepala;
    }

    public String getTanggal_pemeriksaan() {
        return tanggal_pemeriksaan;
    }

    public void setTanggal_pemeriksaan(String tanggal_pemeriksaan) {
        this.tanggal_pemeriksaan = tanggal_pemeriksaan;
    }

    public String getBb() {
        return bb;
    }

    public void setBb(String bb) {
        this.bb = bb;
    }

    public String getTb() {
        return tb;
    }

    public void setTb(String tb) {
        this.tb = tb;
    }

    public String getLingkar_kepala() {
        return lingkar_kepala;
    }

    public void setLingkar_kepala(String lingkar_kepala) {
        this.lingkar_kepala = lingkar_kepala;
    }
}
