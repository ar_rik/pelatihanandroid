package kominfo.android.pelatihan.pelatihanandroid.activity.databayi;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ServerValue;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import kominfo.android.pelatihan.pelatihanandroid.R;
import kominfo.android.pelatihan.pelatihanandroid.activity.BaseActivity;
import kominfo.android.pelatihan.pelatihanandroid.app.EndPoint;
import kominfo.android.pelatihan.pelatihanandroid.app.MyApplication;
import kominfo.android.pelatihan.pelatihanandroid.helper.Const;
import kominfo.android.pelatihan.pelatihanandroid.helper.DatePickerFragment;
import kominfo.android.pelatihan.pelatihanandroid.helper.Utils;
import kominfo.android.pelatihan.pelatihanandroid.model.Bayi;

public class DataBayiPlusActivity extends BaseActivity implements Validator.ValidationListener{
    private String TAG = DataBayiPlusActivity.class.getSimpleName();
    @NotEmpty
    @Bind(R.id.edt_bayi_tambah_nama)
    EditText nama;
    @NotEmpty
    @Bind(R.id.edt_bayi_tambah_tempat)
    EditText tempat;
    @NotEmpty
    @Bind(R.id.edt_bayi_tambah_tanggal)
    EditText tanggal;

    @Bind(R.id.sp_jk)
    Spinner jk;
    private Validator validator;
    private ProgressDialog dia;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_bayi_plus);
        ButterKnife.bind(this);
        validator = new Validator(this);
        validator.setValidationListener(this);
    }

    @OnClick(R.id.btn_tambah_bayi_save)
    public void save(){
        validator.validate();
//        Toast.makeText(DataBayiPlusActivity.this, "Data di tambahkan", Toast.LENGTH_SHORT).show();
    }


    @OnClick(R.id.btn_tambah_bayi_can)
    public void cancle(){
        Toast.makeText(DataBayiPlusActivity.this, "Data gagal ditambahkan", Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.button_cal)
    public void showDatePickerDialog(View v) {
        DatePickerFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    private void saveData() {
//        Toast.makeText(DataBayiPlusActivity.this, "Data di tambahkan", Toast.LENGTH_SHORT).show();
        StringRequest strReq = new StringRequest(Request.Method.POST,
                EndPoint.DATA_BAYI, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.e(TAG, "response: " + response);

                try {
                    JSONObject obj = new JSONObject(response);
                    Log.e(TAG, "response (try): " + obj);
//                    Toast.makeText(DataBayiPlusActivity.this, "Data di tambahkan", Toast.LENGTH_SHORT).show();
                    // check for error
                    if (obj.getBoolean("error") == false) {
                        dia.dismiss();
                        backtohome();
//                        Log.e(TAG, "response (try error): " + obj.getBoolean("error"));
                        JSONObject commentObj = obj.getJSONObject("message");
//                        Toast.makeText(getApplicationContext(), "" + obj.getString("message"), Toast.LENGTH_LONG).show();
                    } else {
                        dia.dismiss();
//                        Log.e(TAG, "response (try error true): " + obj.getBoolean("error"));
//                        Toast.makeText(getApplicationContext(), "" + obj.getString("message"), Toast.LENGTH_LONG).show();
                    }
                    Toast.makeText(getApplicationContext(), "" + obj.getString("message"), Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    Log.e(TAG, "json parsing error(send): " + e.getMessage());
//                    Toast.makeText(getApplicationContext(), "json parse error(send): " + e.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.e(TAG, "Volley error(error response): " + error.getMessage() + ", code: " + networkResponse);
//                Toast.makeText(getApplicationContext(), "Volley error: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }) {

            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
//                params.put("user_id", MyApplication.getInstance().getPrefManager().getUser().getId());
                params.put("name", nama.getText().toString());
                params.put("jk",String.valueOf(jk.getSelectedItemId()));
                params.put("tanggal",tanggal.getText().toString());
                params.put("tempat",tempat.getText().toString());
                params.put("namaibu",nama.getText().toString());
//                params.put("alamat",alamat.getText().toString());

                Log.e(TAG, "Params: " + params.toString());

                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map headers = new HashMap();
                headers.put("Authorization", "746be99d4ef39262e059f3afb7a9cb4f");
                headers.put("Content-Type","application/x-www-form-urlencoded");

                return headers;
            }
        };

        // disabling retry policy so that it won't make
        // multiple http calls
        int socketTimeout = 0;
        RetryPolicy policy = new DefaultRetryPolicy(socketTimeout,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

        strReq.setRetryPolicy(policy);

        //Adding request to request queue
        MyApplication.getInstance().addToRequestQueue(strReq);
    }

    private void backtohome() {
        Intent i = new Intent(getApplicationContext(), DataBayiActivity.class);
        startActivity(i);
        finish();
    }

    @Override
    public void onValidationSucceeded() {
//        dia = ProgressDialog.show(this, null, "Alert Wait");
//        saveData();
        sendDataToServer();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    private void sendDataToServer() {
        /**
         * Create Firebase references
         */
        Firebase userListsRef = new Firebase(Const.FIREBASE_URL_USER_LISTS).child(mEncodedEmail);
        final Firebase firebaseRef = new Firebase(Const.FIREBASE_URL);

        Firebase newListRef = userListsRef.push();

            /* Save listsRef.push() to maintain same random Id */
        final String listId = newListRef.getKey();

            /* HashMap for data to update */
        HashMap<String, Object> updateShoppingListData = new HashMap<>();

        /**
         * Set raw version of date to the ServerValue.TIMESTAMP value and save into
         * timestampCreatedMap
         */
        HashMap<String, Object> timestampCreated = new HashMap<>();
        timestampCreated.put(Const.FIREBASE_PROPERTY_TIMESTAMP, ServerValue.TIMESTAMP);

            /* Build the Bayi list */
        Bayi newBayiList = new Bayi(nama.getText().toString(),tanggal.getText().toString(),"dsfdf",
                String.valueOf(jk.getSelectedItemId()),tempat.getText().toString(), timestampCreated);

        HashMap<String, Object> shoppingListMap = (HashMap<String, Object>)
                new ObjectMapper().convertValue(newBayiList, Map.class);

        Utils.updateMapForAllWithValue(null, listId, mEncodedEmail,
                updateShoppingListData, "", shoppingListMap);

        updateShoppingListData.put("/" + Const.FIREBASE_LOCATION_OWNER_MAPPINGS + "/" + listId,
                mEncodedEmail);

            /* Do the update */
        firebaseRef.updateChildren(updateShoppingListData, new Firebase.CompletionListener() {
            @Override
            public void onComplete(FirebaseError firebaseError, Firebase firebase) {
                    /* Now that we have the timestamp, update the reversed timestamp */
                Utils.updateTimestampReversed(firebaseError, "AddList", listId,
                        null, mEncodedEmail);
            }
        });

        /**
         * Close the dialog fragment when done
         */
        Intent intent = new Intent(getApplicationContext(), DataBayiActivity.class);
        startActivity(intent);
    }
}
