package kominfo.android.pelatihan.pelatihanandroid.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import kominfo.android.pelatihan.pelatihanandroid.R;
import kominfo.android.pelatihan.pelatihanandroid.model.DataDetailBayi;

/**
 * Created by erdearik on 5/2/16.
 */
public class DataDetailBayiAdapter extends RecyclerView.Adapter<DataDetailBayiAdapter.ViewHolder> {

    private Context mContext;
    private ArrayList<DataDetailBayi> allDataDetailBayiArrayList;
    private static String today;
    private final String URL = "http://jerucommerce.com/get_all_students.php";

//    @Override
//    public void onClick(View view) {
////        Toast.makeText(mContext, "The Item Click is:" + get, Toast.LENGTH_SHORT).show();
//    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView id, name, code, bb, tb, lk, status;
        public ImageView photo;
        public RelativeLayout rl;

        public ViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
            code = (TextView) view.findViewById(R.id.count);
            bb = (TextView) view.findViewById(R.id.beratbadan);
            tb = (TextView) view.findViewById(R.id.tinggibadan);
            lk = (TextView) view.findViewById(R.id.lingkarkepala);
            status = (TextView) view.findViewById(R.id.price);
        }

    }


    public DataDetailBayiAdapter(Context mContext, ArrayList<DataDetailBayi> allDataDetailBayiArrayList) {
        this.mContext = mContext;
        this.allDataDetailBayiArrayList = allDataDetailBayiArrayList;

        Calendar calendar = Calendar.getInstance();
        today = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.activity_data_detail_bayi_list, parent, false);

        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        DataDetailBayi allBayi = allDataDetailBayiArrayList.get(position);
        holder.name.setText(allBayi.getTanggal_pemeriksaan());
        holder.code.setText(allBayi.getUmur());
        holder.bb.setText("Berat Badan : "+ allBayi.getBb());
        holder.tb.setText("Tinggi Badan : "+ allBayi.getTb());
        holder.lk.setText("Lingkar Kepala : "+allBayi.getLingkar_kepala());
        holder.status.setText("Kondisi Baik");
    }


    public static String getTimeStamp(String dateStr) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String timestamp = "";

        today = today.length() < 2 ? "0" + today : today;

        try {
            Date date = format.parse(dateStr);
            SimpleDateFormat todayFormat = new SimpleDateFormat("dd");
            String dateToday = todayFormat.format(date);
            format = dateToday.equals(today) ? new SimpleDateFormat("hh:mm a") : new SimpleDateFormat("dd LLL, hh:mm a");
            String date1 = format.format(date);
            timestamp = date1.toString();
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return timestamp;
    }

    @Override
    public int getItemCount() {
        return allDataDetailBayiArrayList.size();
    }

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    public static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private DataDetailBayiAdapter.ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final DataDetailBayiAdapter.ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
}