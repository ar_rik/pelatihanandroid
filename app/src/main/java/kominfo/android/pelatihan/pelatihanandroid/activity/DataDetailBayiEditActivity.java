package kominfo.android.pelatihan.pelatihanandroid.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import butterknife.ButterKnife;
import butterknife.OnClick;
import kominfo.android.pelatihan.pelatihanandroid.R;

public class DataDetailBayiEditActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_detail_bayi_edit);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btn_save)
    public void edit(){

    }

    @OnClick(R.id.btn_can)
    public void cancle(){
        Intent i = new Intent(getApplicationContext(), DataDetailBayiViewActivity.class);
        startActivity(i);
        finish();
    }
}
