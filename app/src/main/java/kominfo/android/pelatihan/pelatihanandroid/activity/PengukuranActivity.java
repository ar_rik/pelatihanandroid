package kominfo.android.pelatihan.pelatihanandroid.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import butterknife.Bind;
import kominfo.android.pelatihan.pelatihanandroid.R;
import kominfo.android.pelatihan.pelatihanandroid.activity.soal.SoalBulanEnamActivity;
import kominfo.android.pelatihan.pelatihanandroid.activity.soal.SoalBulanTigaActivity;


public class PengukuranActivity extends AppCompatActivity {
int umur = 3;

    @Bind(R.id.buttonPengukuran)
    Button butonPengukuran;



    public final static String EXTRA_MESSAGE = "kominfo.android.pelatihan.pelatihanandroid.MESSAGE";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pengukuran);
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        setSupportActionBar(toolbar);

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//        fab.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
//            }
//        });


    }
    public void sendMessage(View view) {

        if((umur >=3)&&(umur<6)) {
            Intent intent = new Intent(this, SoalBulanTigaActivity.class);
//                EditText editText = (EditText) findViewById(R.id.edit_message);
//                String message = editText.getText().toString();
            intent.putExtra(EXTRA_MESSAGE, String.valueOf(umur));
            startActivity(intent);
        }
        else if((umur >=6)&&(umur<9)){
            Intent intent = new Intent(this, SoalBulanEnamActivity.class);
//                EditText editText = (EditText) findViewById(R.id.edit_message);
//                String message = editText.getText().toString();
            intent.putExtra(EXTRA_MESSAGE, String.valueOf(umur));
            startActivity(intent);
        }
    }



}
