package kominfo.android.pelatihan.pelatihanandroid.kelasPengolahanData;

/**
 * Created by dera on 30/05/2016.
 */
public class GPPH {
    int p1,p2,p3,p4,p5,p6,p7,p8,p9,p10;
    public GPPH(int j1,int j2,int j3,int j4,int j5,int j6,int j7,int j8,int j9,int j10,int j11,int j12){
        p1=j1;
        p2=j2;
        p3=j3;
        p4=j4;
        p5=j5;
        p6=j6;
        p7=j7;
        p8=j8;
        p9=j9;
        p10=j10;

    }
    public String[] statusGPPH (){
        //method untuk mendapatkan status GPPH dengan Output ada 2 yaitu status GPPH dan intervensi
        String statusGPPH="";
        String intervensi="";
        int jumlahNilai = p1+p2+p3+p4+p5+p6+p7+p8+p9+p10;
        if (jumlahNilai >= 13){
            statusGPPH = "Kemungkinan memiliki GPPH";
            intervensi = "Harus dibawa ke rumah sakit dengan fasilitas kesehatan jiwa/tumbuh kembang anak";
        }
        else{
            statusGPPH = "Normal";
            intervensi = "Jika ragu-ragu, lakukan pemeriksaan ulang 1 bulan kemudian dan ajukan pertanyaan ke orang disekitar anak";
        }
        return new String[]{statusGPPH,intervensi};
    }
}
