package kominfo.android.pelatihan.pelatihanandroid.model;

/**
 * Created by erdearik on 5/2/16.
 */
public class Student {
    private String id, name, code, create, update;

    public Student(String id, String name, String code, String create, String update) {
        this.id = id;
        this.name = name;
        this.code = code;
        this.create = create;
        this.update = update;
    }

    public Student() {

    }

    public String getId() {

        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getCreate() {
        return create;
    }

    public void setCreate(String create) {
        this.create = create;
    }

    public String getUpdate() {
        return update;
    }

    public void setUpdate(String update) {
        this.update = update;
    }
}
